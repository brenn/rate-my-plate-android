package com.publica.ratemyplate.data


class Constant {

    companion object {
        const val OK_CODE = 200
        const val PACKAGE = "package"
        const val LIKE = 1
        const val DISLIKE = -1
        const val NEUTRAL = 0
        const val DEFAULT = 2
        const val FAMILY = 1
        const val OTHERS = 0
        const val COMMENT = 1

        const val FEMALE = "Female"
        const val MALE = "Male"
        const val OTHER = "Other"
    }



}

