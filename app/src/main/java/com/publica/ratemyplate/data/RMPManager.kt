
import com.publica.ratemyplate.model.MyPlate
import com.publica.ratemyplate.model.PlateNumber
import com.publica.ratemyplate.model.Profile

object RMPManager {

    var plateNumber: List<PlateNumber.Datum>? = null
    var searchPlateNumber: String? = ""

    var family: ArrayList<MyPlate.Other>? = null
    var others: ArrayList<MyPlate.Other>? = null

    var selectedPlateNumber: MyPlate.Other? = null
    var profile: Profile? = null
    var code: String? = null
    var screen: String? = null
}