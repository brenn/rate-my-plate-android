package com.publica.ratemyplate.data

import android.content.Context
import android.content.SharedPreferences

object Preference {

    private const val PREFERENCE = "prefs"
    private const val FIRST_LOAD = "first_load"
    private const val IS_LOGIN = "is_login"
    private const val TOKEN = "token"
    private const val USER_ID = "user_id"
    private const val REMEMBER_ME = "remember_me"
    private const val EMAIL = "email"

    private const val FIRST_RATE = "first_rate"
    private const val FIRST_ADD = "first_add"

    private var mSharedPreferences: SharedPreferences? = null
    private var mEditor: SharedPreferences.Editor? = null

    fun initPreference(context: Context) {
        mSharedPreferences = context.getSharedPreferences(PREFERENCE, Context.MODE_PRIVATE)
    }

    fun clearPreference() {
        mSharedPreferences!!.edit().clear().apply()
    }

    var isFirstLoad: Boolean
        get() = mSharedPreferences!!.getBoolean(FIRST_LOAD, true)
        set(data) {
            mEditor = mSharedPreferences!!.edit()
            mEditor!!.putBoolean(FIRST_LOAD, data)
            mEditor!!.apply()
        }

    var isLogin: Boolean
        get() = mSharedPreferences!!.getBoolean(IS_LOGIN, false)
        set(data) {
            mEditor = mSharedPreferences!!.edit()
            mEditor!!.putBoolean(IS_LOGIN, data)
            mEditor!!.apply()
        }

    var token: String
        get() = mSharedPreferences!!.getString(TOKEN, "")!!
        set(data) {
            mEditor = mSharedPreferences!!.edit()
            mEditor!!.putString(TOKEN, data)
            mEditor!!.apply()
        }


    var userID: Int
        get() = mSharedPreferences!!.getInt(USER_ID, -1)
        set(data) {
            mEditor = mSharedPreferences!!.edit()
            mEditor!!.putInt(USER_ID, data)
            mEditor!!.apply()
        }

    var rememberMe: Boolean
        get() = mSharedPreferences!!.getBoolean(REMEMBER_ME, false)
        set(data) {
            mEditor = mSharedPreferences!!.edit()
            mEditor!!.putBoolean(REMEMBER_ME, data)
            mEditor!!.apply()
        }

    var email: String
        get() = mSharedPreferences!!.getString(EMAIL, "")!!
        set(data) {
            mEditor = mSharedPreferences!!.edit()
            mEditor!!.putString(EMAIL, data)
            mEditor!!.apply()
        }

    var firstRate: Boolean
        get() = mSharedPreferences!!.getBoolean(FIRST_RATE, true)
        set(data) {
            mEditor = mSharedPreferences!!.edit()
            mEditor!!.putBoolean(FIRST_RATE, data)
            mEditor!!.apply()
        }

    var firstAdd: Boolean
        get() = mSharedPreferences!!.getBoolean(FIRST_ADD, true)
        set(data) {
            mEditor = mSharedPreferences!!.edit()
            mEditor!!.putBoolean(FIRST_ADD, data)
            mEditor!!.apply()
        }

}