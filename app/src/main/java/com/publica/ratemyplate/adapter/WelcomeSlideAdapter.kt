package com.publica.ratemyplate.adapter

import android.content.Context
import android.content.Intent
import android.graphics.Typeface
import android.support.v4.app.FragmentActivity
import android.support.v4.view.PagerAdapter
import android.support.v4.view.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import com.publica.ratemyplate.activity.AccountActivity
import com.publica.ratemyplate.activity.MainActivity
import com.publica.ratemyplate.data.Preference
import kotlinx.android.synthetic.main.slide_welcome_3.view.*

class WelcomeSlideAdapter(private val activity: FragmentActivity, private val layouts: IntArray, private val listener: SlideAdapterInterface) : PagerAdapter() {

    private val last_page = 2

    var viewPagerChangeListener: ViewPager.OnPageChangeListener = object : ViewPager.OnPageChangeListener {
        override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

        }

        override fun onPageSelected(position: Int) {
            var isLastPage = false

            if (position == layouts.size - 1) {
                isLastPage = true
            }

            listener.onButtonChanged(isLastPage, position)

        }

        override fun onPageScrollStateChanged(state: Int) {

        }
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val layoutInflater = activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = layoutInflater.inflate(layouts[position], container, false)

        if(layouts[position] == layouts[last_page]){

            view.finish.setOnClickListener {

                Preference.isFirstLoad = false
                activity.startActivity(Intent(activity.applicationContext, AccountActivity::class.java))
                activity.finish()

            }

        }

        container.addView(view)
        return view

    }

    override fun getCount(): Int {
        return layouts.size
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object`
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        val view = `object` as View
        container.removeView(view)
    }

    interface SlideAdapterInterface {
        fun onButtonChanged(isLastPage: Boolean, position: Int)
        fun onButtonNextPage(position: Int)
    }

    private fun setExternalFonts(editText: EditText) {
        val tf = Typeface.createFromAsset(activity.getAssets(),
            "fonts/OpenSans-Light.ttf")
        editText.setTypeface(tf)
    }

    private fun setExternalFonts(button: Button) {
        val tf = Typeface.createFromAsset(activity.getAssets(),
            "fonts/OpenSans-SemiBold.ttf")
        button.setTypeface(tf)
    }

}