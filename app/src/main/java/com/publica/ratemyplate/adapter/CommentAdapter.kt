package com.publica.ratemyplate.adapter


import android.app.AlertDialog
import android.content.DialogInterface
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.google.gson.Gson
import com.publica.ratemyplate.R
import com.publica.ratemyplate.activity.MainActivity
import com.publica.ratemyplate.data.Constant
import com.publica.ratemyplate.data.Preference
import com.publica.ratemyplate.model.Comment
import com.publica.ratemyplate.model.ErrorMessage
import com.publica.ratemyplate.model.Moderate
import com.publica.ratemyplate.network.APIClient
import com.publica.ratemyplate.network.APIInterface
import com.publica.ratemyplate.ui.CustomFontButton
import com.publica.ratemyplate.ui.CustomFontTextView
import com.publica.ratemyplate.ui.RMPDialog
import com.publica.ratemyplate.viewholder.CommentViewHolder
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.dialog_image_preview.view.*
import kotlinx.android.synthetic.main.dialog_message.view.*
import kotlinx.android.synthetic.main.dialog_moderation_dialog.view.*
import retrofit2.Call
import retrofit2.Response

class CommentAdapter(
    private val activity: MainActivity,
    private var commentList: ArrayList<Comment>,
    private val commentCounter: CustomFontTextView
) :
    RecyclerView.Adapter<CommentViewHolder>() {

    private var mainList = commentList

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CommentViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.list_item_comment, parent, false)

        return CommentViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: CommentViewHolder, position: Int) {

        val commentItems = mainList[position]
        val attachments = commentItems.commentAttachments

        holder.commentTV.text = commentItems.comment
        holder.commentDateTv.text = commentItems.createdDate

        val imgDisplays = ArrayList<ImageView>()
        imgDisplays.add(holder.img1Display)
        imgDisplays.add(holder.img2Display)
        imgDisplays.add(holder.img3Display)

        if (attachments != null && attachments.isNotEmpty()) {

            holder.img1Display.setOnClickListener {
                if(attachments.size > 0)
                imagePreviewDialog(attachments[0].image!!)
            }

            holder.img2Display.setOnClickListener{
                if(attachments.size > 1)
                imagePreviewDialog(attachments[1].image!!)
            }

            holder.img3Display.setOnClickListener{

                if(attachments.size > 2)
                imagePreviewDialog(attachments[2].image!!)
            }

            var ctr = 0
            for (i in attachments) {
                if (ctr < 3) {
                    Picasso.with(activity).load(attachments[ctr].thumbnail).into(imgDisplays[ctr])
                    ctr++
                }

            }

        } else {
            holder.img1Display.visibility = View.GONE
            holder.img2Display.visibility = View.GONE
            holder.img3Display.visibility = View.GONE
        }

        if (Preference.userID == commentItems.userId) {
            holder.reviewerTV.text = activity.getString(R.string.your_review)
            holder.commentTopLayout.setBackgroundResource(R.drawable.yourcommentheader)


            holder.deleteModerateBtn.setImageResource(R.drawable.smalldelete)
            holder.deleteModerateBtn.visibility = View.VISIBLE


        } else {
            holder.commentTopLayout.setBackgroundResource(R.drawable.commentheader)
            holder.deleteModerateBtn.setImageResource(R.drawable.smallreport)
            holder.deleteModerateBtn.visibility = View.GONE

            if(commentItems.user != null){

                holder.reviewerTV.text = commentItems.user!!.nickname
            }else{
                holder.reviewerTV.text = "unknown"
            }

            if(commentItems.moderateComment != null){

                if(commentItems.moderateComment!!.moderated == 0){

                    holder.commentTV.text = activity.getString(R.string.under_consideration)
                }
                holder.deleteModerateBtn.visibility = View.INVISIBLE

            }

        }

        holder.deleteModerateBtn.setOnClickListener {

            if (Preference.userID == commentItems.userId) {
//                delete
                defaultConditionDialog( activity.getString(R.string.delete_comment_confirmation), commentItems.id!!, true, position)
            } else {
                //moderation
                defaultConditionDialog(activity.getString(R.string.report_comment_confirmation), commentItems.id!!, false, position)
            }

        }

        val obj = object : CommentViewHolder.OnViewClickListener {
            override fun onViewClick(view: View, comment: Comment?, position: Int) {

            }
        }

        holder.setOnViewClickListener(obj, commentItems, position)

    }

    override fun getItemCount(): Int {

        if(mainList.size <= 2){
            return mainList.size
        }else{
            return 2
        }

    }

    private fun deleteComment(id: Int, position: Int) {

        val apiInterface = APIClient.getClient(activity).create(APIInterface::class.java)
        val call = apiInterface.deleteComment(id)
        call.enqueue(object : retrofit2.Callback<Comment> {

            override fun onResponse(call: Call<Comment>, response: Response<Comment>) {

                if (response.code() != Constant.OK_CODE) {

                    val resourceError = response.errorBody()
                    val errorMessage = Gson().fromJson(resourceError!!.string(), ErrorMessage::class.java)

                    if (errorMessage.message == activity.getString(R.string.unauthenticated)) {

                        activity.startLoginActivity()
                    }
                } else {

                    mainList.removeAt(position)
                    notifyDataSetChanged()

                    var commentCtr = "Comment"
                    var commentSize = mainList.size

                    if (commentSize == 1) {
                        commentCtr = "$commentCtr $commentSize"
                    } else if (commentSize > 1) {
                        commentCtr = commentCtr + "s " + commentSize
                    }

                    commentCounter.text = commentCtr

                }
            }

            override fun onFailure(call: Call<Comment>, t: Throwable) {
                if(activity != null){
                    RMPDialog.defaultDialog(activity, activity.getString(R.string.error_mes))
                }
            }

        })
    }

    private fun moderateComment(id: Int, position: Int) {

        val apiInterface = APIClient.getClient(activity).create(APIInterface::class.java)
        val call = apiInterface.postModerateComment(Comment(id))
        call.enqueue(object : retrofit2.Callback<Moderate> {

            override fun onResponse(call: Call<Moderate>, response: Response<Moderate>) {

                if (response.code() != Constant.OK_CODE) {

                    val resourceError = response.errorBody()
                    val errorMessage = Gson().fromJson(resourceError!!.string(), ErrorMessage::class.java)

                    if (errorMessage.message == activity.getString(R.string.unauthenticated)) {
                        activity.startLoginActivity()
                    }
                } else {

                    val resource = response.body()
                    val data = resource!!.data!!

                    mainList.get(position).moderateComment = Moderate(data.id!!, data.commentID!!, data.userId!!, data.createdAt!!, data.updatedAt!!, 0)
                    notifyDataSetChanged()
                    RMPDialog.defaultDialog(activity, activity.getString(R.string.report_has_been_submitted))

                }
            }

            override fun onFailure(call: Call<Moderate>, t: Throwable) {
                if(activity != null){
                    RMPDialog.defaultDialog(activity, activity.getString(R.string.error_mes))
                }
            }

        })
    }

    private fun defaultConditionDialog(message: String, id: Int, isDelete: Boolean, position: Int) {

        val builder = AlertDialog.Builder(activity)
        builder.setPositiveButton(
            activity.getString(R.string.ok),
            DialogInterface.OnClickListener { dialog, which ->

                if (isDelete) {
                    deleteComment(id, position)
                } else {
                    moderateCommentDialog(id, position)

                }

            })

        builder.setNegativeButton(activity.getString(R.string.cancel), null)

        val inflater = activity.layoutInflater
        val view = inflater.inflate(R.layout.dialog_message, null)
        view.dialog_mess.text = message

        val dialog = builder.create()
        dialog.setView(view)
        dialog.show()

    }

    private fun moderateCommentDialog(id: Int, position: Int){

        val builder = AlertDialog.Builder(activity)

        val inflater = activity.layoutInflater
        val view = inflater.inflate(R.layout.dialog_moderation_dialog, null)



        builder.setPositiveButton(
            activity.getString(R.string.send),
            DialogInterface.OnClickListener { dialog, which ->
                val reason = view.reason.text.toString()

                moderateComment(id, position)

            })

        builder.setNegativeButton(activity.getString(R.string.cancel), null)



        val dialog = builder.create()
        dialog.setView(view)
        dialog.show()
    }

    fun updateList(list: ArrayList<Comment>, commentTxt: CustomFontTextView, unlockTxt: CustomFontTextView, notAvailableTxt: CustomFontTextView, viewAllBtn: CustomFontButton) {
        mainList = list
        notifyDataSetChanged()

        var commentCtr = "Comment"
        var commentSize = mainList.size

        if (commentSize == 1) {
            commentCtr = "$commentCtr $commentSize"
        } else if (commentSize > 1) {
            commentCtr = commentCtr + "s " + commentSize
        }

        val mes_1 = activity.getString(R.string.unlock_comments_mes_1)
        val mes_2 = activity.getString(R.string.unlock_comments_mes_2)
        val hiddenCommentSize = (commentSize - 2)


        commentTxt.text = commentCtr

        if(commentSize <= 2){
            unlockTxt.visibility = View.GONE
            viewAllBtn.visibility = View.GONE
            notAvailableTxt.visibility = View.GONE
        }else{
            unlockTxt.text = "$mes_1 $hiddenCommentSize $mes_2"
            unlockTxt.visibility = View.VISIBLE
            viewAllBtn.visibility = View.VISIBLE
            notAvailableTxt.visibility = View.VISIBLE
        }

    }

    private fun imagePreviewDialog(imageUrl: String) {

        val builder = AlertDialog.Builder(activity)


        builder.setNegativeButton(activity.getString(R.string.cancel), null)

        val inflater = activity.layoutInflater
        val view = inflater.inflate(R.layout.dialog_image_preview, null)

        Picasso.with(activity).load(imageUrl).into(view.image_prev)

        val dialog = builder.create()
        dialog.setView(view)
        dialog.show()

    }

}