package com.publica.ratemyplate.adapter


import android.app.AlertDialog
import android.content.DialogInterface
import android.graphics.Color
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.gson.Gson

import com.publica.ratemyplate.R
import com.publica.ratemyplate.activity.MainActivity
import com.publica.ratemyplate.data.Constant

import com.publica.ratemyplate.data.Preference
import com.publica.ratemyplate.model.ErrorMessage


import com.publica.ratemyplate.model.PlateNumber
import com.publica.ratemyplate.network.APIClient
import com.publica.ratemyplate.network.APIInterface
import com.publica.ratemyplate.ui.RMPDialog
import com.publica.ratemyplate.util.Util

import com.publica.ratemyplate.viewholder.ReviewViewHolder
import kotlinx.android.synthetic.main.dialog_message.view.*
import retrofit2.Call
import retrofit2.Response


class ReviewAdapter(

    private val activity: MainActivity,
    private val mainList: List<PlateNumber.Datum>,
    eventListener: EventListener

) :
    RecyclerView.Adapter<ReviewViewHolder>() {

    var listener: EventListener? = eventListener

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReviewViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.list_item_review, parent, false)

        return ReviewViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ReviewViewHolder, position: Int) {

        if(mainList == null)
            return

        val reviewItems = mainList[0].ratings!![position]
//        val splitDate = reviewItems.createdAt!!.date!!.split(" ")
        val splitRatingDateTime = Util.epochToDateAndTime(reviewItems.timing!!.toLong()).split(" ")
        holder.reviewDate.text = Util.epochToDate(reviewItems.createdAt!!.toLong())
        holder.date.text = splitRatingDateTime[0]
        holder.time.text = splitRatingDateTime[1]
        holder.category.text = reviewItems.category
        holder.location.text = reviewItems.location
        holder.reviewer.text = reviewItems.username

        if (reviewItems.rating == Constant.LIKE) {
            holder.ratingImg.setImageResource(R.drawable.platesmalllpositiveicon)
        } else if (reviewItems.rating == Constant.DISLIKE) {
            holder.ratingImg.setImageResource(R.drawable.platesmallnegativeicon)
        } else if (reviewItems.rating == Constant.NEUTRAL) {
            holder.ratingImg.setImageResource(R.drawable.platesmallneutralicon)
        }

        if (Preference.userID == reviewItems.userId) {
            holder.reviewer.text = activity.getString(R.string.your_review)
            holder.reviewer.setTextColor(ContextCompat.getColor(activity, R.color.white))
            holder.reviewDate.setTextColor(ContextCompat.getColor(activity, R.color.white))
            holder.commentTopLayout.setBackgroundResource(R.drawable.yourcommentheader)
            holder.deleteLayout.visibility = View.VISIBLE

        } else {
            holder.commentTopLayout.setBackgroundResource(R.drawable.commentheader)
            holder.deleteLayout.visibility = View.GONE
            holder.reviewer.setTextColor(ContextCompat.getColor(activity, R.color.rmp_dark_gray))
            holder.reviewDate.setTextColor(ContextCompat.getColor(activity, R.color.rmp_dark_gray))
//            if(commentItems.user != null){
//
//                holder.reviewerTV.text = commentItems.user!!.nickname
//            }else{
//                holder.reviewerTV.text = "unknown"
//            }


        }

        holder.deleteLayout.setOnClickListener {
            defaultConditionDialog(reviewItems.id!!)
        }

        val obj = object : ReviewViewHolder.OnViewClickListener {
            override fun onViewClick(view: View, review: PlateNumber.Rating?, position: Int) {

            }
        }

        holder.setOnViewClickListener(obj, reviewItems, position)

    }

    override fun getItemCount(): Int {

      if(mainList[0].ratings!![0].userId == Preference.userID && mainList[0].ratings!!.size > 2){
          return 3
      }else if (mainList[0].ratings!!.size <= 2) {
            return mainList[0].ratings!!.size
        } else {
            return 2
        }
    }


    private fun defaultConditionDialog(id: Int) {

        val builder = AlertDialog.Builder(activity)
        builder.setPositiveButton(
            activity.getString(R.string.delete),
            DialogInterface.OnClickListener { _, _ ->

                deleteRating(id)

            })

        builder.setNegativeButton(activity.getString(R.string.cancel), null)

        val inflater = activity.layoutInflater
        val view = inflater.inflate(R.layout.dialog_message, null)
        view.dialog_mess.text = activity.getString(R.string.delete_rating_mes)

        val dialog = builder.create()
        dialog.setView(view)
        dialog.show()

    }

    private fun deleteRating(id: Int) {

        val apiInterface = APIClient.getClient(activity).create(APIInterface::class.java)
        val call = apiInterface.deleteRating(id)
        call.enqueue(object : retrofit2.Callback<PlateNumber> {

            override fun onResponse(call: Call<PlateNumber>, response: Response<PlateNumber>) {

                if (response.code() != Constant.OK_CODE) {

                    val resourceError = response.errorBody()
                    val errorMessage = Gson().fromJson(resourceError!!.string(), ErrorMessage::class.java)


                    if (errorMessage.message == activity.getString(R.string.unauthenticated)) {

                        activity.startLoginActivity()
                    } else {
                        RMPDialog.defaultDialog(activity, errorMessage.message!!)
                    }

                } else {

                    val resource = response.body()
                    RMPDialog.defaultDialog(activity, resource!!.message!!)
                    listener!!.onEvent()
                }
            }

            override fun onFailure(call: Call<PlateNumber>, t: Throwable) {
                if(activity != null){
                    RMPDialog.defaultDialog(activity, activity.getString(R.string.error_mes))
                }

            }

        })

    }

    interface EventListener {
        fun onEvent()
    }

}