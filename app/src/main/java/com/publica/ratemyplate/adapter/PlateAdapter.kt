package com.publica.ratemyplate.adapter

import android.app.AlertDialog
import android.content.DialogInterface
import android.support.v4.app.FragmentActivity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.daimajia.swipe.SwipeLayout
import com.google.gson.Gson
import com.publica.ratemyplate.R
import com.publica.ratemyplate.activity.MainActivity
import com.publica.ratemyplate.data.Constant
import com.publica.ratemyplate.fragment.EditPlateFragment
import com.publica.ratemyplate.fragment.NewMyPlateDetails
import com.publica.ratemyplate.model.*
import com.publica.ratemyplate.network.APIClient
import com.publica.ratemyplate.network.APIInterface
import com.publica.ratemyplate.ui.RMPDialog

import com.publica.ratemyplate.viewholder.PlateViewHolder
import kotlinx.android.synthetic.main.dialog_message.view.*
import retrofit2.Call
import retrofit2.Response

class PlateAdapter(private val activity: FragmentActivity, private val plateList: ArrayList<MyPlate.Other>) :
    RecyclerView.Adapter<PlateViewHolder>() {

    private var mainPlateList = plateList
    private var selectedHolder: PlateViewHolder? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlateViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.list_item_my_plate, parent, false)

        return PlateViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: PlateViewHolder, position: Int) {

        if (mainPlateList == null)
            return


        val plateItem = mainPlateList[position]

        var upVote = 0
        var downVote = 0
        var neutral = 0


        if (plateItem.plateNumber!!.ratings != null) {

            for (rating in plateItem.plateNumber!!.ratings!!) {

                if (rating.rating == Constant.LIKE) {
                    upVote++
                } else if (rating.rating == Constant.DISLIKE) {
                    downVote++
                } else {
                    neutral++
                }
            }

        }

        holder.plateNumber.text = plateItem.plateNumber!!.plateNumber
        holder.plateName.text = plateItem.name
//        holder.comment.text = plateItem.plateNumber!!.comments!!.size.toString()
        holder.alert.text = plateItem.my_alerts!!.size.toString()
        holder.upVote.text = upVote.toString()
        holder.downVote.text = downVote.toString()
        holder.neutral.text = neutral.toString()
        holder.swipe1.showMode = SwipeLayout.ShowMode.PullOut
        holder.swipe1.addDrag(SwipeLayout.DragEdge.Right, holder.bottomWrapper)



        holder.deleteLayout.setOnClickListener {
            defaultConditionDialog(activity.getString(R.string.delete_plate_number_mes), plateItem.id!!, position)
        }

        holder.editLayout.setOnClickListener {
            RMPManager.selectedPlateNumber = plateItem
            EditPlateFragment.show(activity)
        }

        holder.plateNumberCardLayout.setOnClickListener {
            RMPManager.selectedPlateNumber = plateItem
            RMPManager.searchPlateNumber = plateItem.plateNumber!!.plateNumber
            selectedHolder = holder
//            PlateDetailsFragment.show(activity, activity.getString(R.string.nav_my_plates))
            NewMyPlateDetails.show(activity, activity.getString(R.string.nav_my_plates))
        }

        val obj = object : PlateViewHolder.OnViewClickListener {
            override fun onViewClick(view: View, plate: MyPlate.Other?, position: Int) {

            }
        }

        holder.setOnViewClickListener(obj, plateItem, position)

    }

    override fun getItemCount(): Int {
        return mainPlateList.size
    }

    private fun deletePlateNumber(id: Int, position: Int) {

        val apiInterface = APIClient.getClient(activity).create(APIInterface::class.java)
        val call = apiInterface.deletePlateOnMyList(id)
        call.enqueue(object : retrofit2.Callback<PlateNumber> {

            override fun onResponse(call: Call<PlateNumber>, response: Response<PlateNumber>) {

                if (response.code() != Constant.OK_CODE) {

                    val resourceError = response.errorBody()
                    val errorMessage = Gson().fromJson(resourceError!!.string(), ErrorMessage::class.java)
                    val act = activity as MainActivity
                    if (errorMessage.message == act.getString(R.string.unauthenticated)) {

                        act.startLoginActivity()
                    } else {
                        RMPDialog.defaultDialog(activity, errorMessage.message!!)
                    }

                } else {

                    val resource = response.body()
                    RMPDialog.defaultDialog(activity, resource!!.message!!)

                    mainPlateList.removeAt(position)
                    notifyDataSetChanged()

                }
            }

            override fun onFailure(call: Call<PlateNumber>, t: Throwable) {
                if (activity != null) {
                    RMPDialog.defaultDialog(activity, activity.getString(R.string.error_mes))
                }
            }

        })

    }

    private fun defaultConditionDialog(message: String, id: Int, position: Int) {

        val builder = AlertDialog.Builder(activity)
        builder.setPositiveButton(
            activity.getString(R.string.delete),
            DialogInterface.OnClickListener { dialog, which ->

                deletePlateNumber(id, position)

            })

        builder.setNegativeButton(activity.getString(R.string.cancel), null)

        val inflater = activity.layoutInflater
        val view = inflater.inflate(R.layout.dialog_message, null)
        view.dialog_mess.text = message

        val dialog = builder.create()
        dialog.setView(view)
        dialog.show()

    }


    fun updateList(list: ArrayList<MyPlate.Other>) {
        mainPlateList = list
        notifyDataSetChanged()
    }

}