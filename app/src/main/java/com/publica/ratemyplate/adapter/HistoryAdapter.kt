package com.publica.ratemyplate.adapter

import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.DialogInterface
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.daimajia.swipe.SwipeLayout
import com.google.gson.Gson
import com.publica.ratemyplate.R
import com.publica.ratemyplate.activity.MainActivity
import com.publica.ratemyplate.data.Constant
import com.publica.ratemyplate.fragment.NewMyPlateDetails
import com.publica.ratemyplate.fragment.PlateDetails2Fragment
import com.publica.ratemyplate.model.ErrorMessage
import com.publica.ratemyplate.model.History
import com.publica.ratemyplate.model.PlateNumber
import com.publica.ratemyplate.network.APIClient
import com.publica.ratemyplate.network.APIInterface
import com.publica.ratemyplate.ui.RMPDialog
import com.publica.ratemyplate.viewholder.HistoryViewHolder
import kotlinx.android.synthetic.main.dialog_message.view.*
import retrofit2.Call
import retrofit2.Response

class HistoryAdapter(
    private val activity: MainActivity,
    private val historyList: ArrayList<History.Datum>,
    eventListener: EventListener
) : RecyclerView.Adapter<HistoryViewHolder>() {


    var listener: EventListener? = eventListener
    private lateinit var progressDialog: ProgressDialog

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HistoryViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.list_item_history, parent, false)

        return HistoryViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: HistoryViewHolder, position: Int) {

        if(historyList == null)
            return

        val historyItem = historyList[position]

        if (historyItem.comment_id == null) {
            if (historyItem.rating_value == Constant.LIKE) {
                //like
                holder.actionImg.setImageResource(R.drawable.ratedpositiveicon)
            }else if(historyItem.rating_value == Constant.NEUTRAL){
                holder.actionImg.setImageResource(R.drawable.ratedneutralicon)
            }
            else {
                //dislike
                holder.actionImg.setImageResource(R.drawable.ratednegativeicon)
            }
        } else {
            //comment
        }

        if(historyItem.plate_number == null)
            return

        holder.plateNumber.text = historyItem.plate_number!!.plateNumber
        holder.swipe1.showMode = SwipeLayout.ShowMode.PullOut
        holder.swipe1.addDrag(SwipeLayout.DragEdge.Right, holder.bottomWrapper)
        holder.swipe1.isSwipeEnabled = false

        holder.delete.setOnClickListener {
            //            deletePlateNumber(historyItem.id!!)
            defaultConditionDialog(historyItem.id!!)

        }

        holder.swipeCard.setOnClickListener {

            //todo

//            progressDialog = ProgressDialog.show(
//                activity,
//                activity.getString(R.string.please_wait),
//                activity.getString(R.string.getting_data),
//                true
//            )

            val pn = ArrayList<PlateNumber.Datum>()

            pn.add(historyItem.plate_number!!)

            RMPManager.plateNumber = pn
//            PlateDetails2Fragment.show(activity, activity.getString(R.string.nav_my_plates))

            RMPManager.searchPlateNumber = pn[0].plateNumber
            NewMyPlateDetails.show(activity, activity.getString(R.string.nav_my_plates))

//            getPlateDetails(historyItem.plate_number!!.plateNumber!!)


//            for(family in RMPManager.family!!){
//                if(family.plateNumber!!.plateNumber == historyItem.plate_number){
//
//                    RMPManager.selectedPlateNumber = family
//
//                    PlateDetailsFragment.show(activity, activity.getString(R.string.nav_history))
//
//                    return@setOnClickListener
//                }
//            }

//            for(other in RMPManager.others!!){
//                if(other.plateNumber!!.plateNumber == historyItem.plate_number){
//
//                    RMPManager.selectedPlateNumber = other
//
//                    PlateDetailsFragment.show(activity, activity.getString(R.string.nav_history))
//
//                    return@setOnClickListener
//                }
//            }

        }

        val obj = object : HistoryViewHolder.OnViewClickListener {
            override fun onViewClick(view: View, history: History.Datum?, position: Int) {

            }
        }

        holder.setOnViewClickListener(obj, historyItem, position)

    }

    override fun getItemCount(): Int {
        return historyList.size
    }

    private fun deletePlateNumber(id: Int) {

        val apiInterface = APIClient.getClient(activity).create(APIInterface::class.java)
        val call = apiInterface.deleteHistory(id)
        call.enqueue(object : retrofit2.Callback<History> {

            override fun onResponse(call: Call<History>, response: Response<History>) {

                if (response.code() != Constant.OK_CODE) {

                    val resourceError = response.errorBody()
                    val errorMessage = Gson().fromJson(resourceError!!.string(), ErrorMessage::class.java)



                    if (errorMessage.message == activity.getString(R.string.unauthenticated)) {

                        activity.startLoginActivity()
                    } else {
                        RMPDialog.defaultDialog(activity, errorMessage.message!!)
                    }

                } else {

                    val resource = response.body()
                    RMPDialog.defaultDialog(activity, resource!!.message!!)
                    listener!!.onEvent()

                }
            }

            override fun onFailure(call: Call<History>, t: Throwable) {
                if (activity != null) {
                    RMPDialog.defaultDialog(activity, activity.getString(R.string.error_mes))
                }
            }

        })

    }

    interface EventListener {
        fun onEvent()
    }

    private fun getPlateDetails(plateNumber: String) {
        val apiInterface = APIClient.getClient(activity).create(APIInterface::class.java)
        val call = apiInterface.getSearchPlate(
            plateNumber,
            "ratings",
            "comments",
            "comments.user",
            "comments.moderate_comment"
        )
        call.enqueue(object : retrofit2.Callback<PlateNumber> {

            override fun onResponse(call: Call<PlateNumber>, response: Response<PlateNumber>) {
                progressDialog.dismiss()
                val myPlateResponse = response.body()
                val message = myPlateResponse?.message

                if (message != null) {

                    if (message == activity.getString(R.string.unauthenticated)) {
                        activity.startLoginActivity()
                    } else {
                        //Proceed
                    }

                } else {
                    val data = myPlateResponse?.data

                    RMPManager.plateNumber = data
//                    RMPManager.searchPlateNumber = view.plate_number_et.text.toString()
                    PlateDetails2Fragment.show(activity, activity.getString(R.string.nav_my_plates))

                }

            }

            override fun onFailure(call: Call<PlateNumber>, t: Throwable) {

                if(activity != null){
                    progressDialog.dismiss()
                    RMPDialog.defaultDialog(activity, activity.getString(R.string.error_mes))
                }

            }

        })
    }

    private fun defaultConditionDialog(id: Int) {

        val builder = AlertDialog.Builder(activity)
        builder.setPositiveButton(
            activity.getString(R.string.delete),
            DialogInterface.OnClickListener { dialog, which ->

                deletePlateNumber(id)

            })

        builder.setNegativeButton(activity.getString(R.string.cancel), null)

        val inflater = activity.layoutInflater
        val view = inflater.inflate(R.layout.dialog_message, null)
        view.dialog_mess.text = activity.getString(R.string.delete_history_mes)

        val dialog = builder.create()
        dialog.setView(view)
        dialog.show()

    }

}