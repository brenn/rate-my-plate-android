package com.publica.ratemyplate.adapter

import android.support.v4.app.FragmentActivity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.publica.ratemyplate.R
import com.publica.ratemyplate.fragment.*
import com.publica.ratemyplate.model.ProfileSettings
import com.publica.ratemyplate.ui.RMPDialog
import com.publica.ratemyplate.util.Util
import com.publica.ratemyplate.viewholder.SettingsViewHolder


class SettingsAdapter(private val activity: FragmentActivity, private val profileSettings: List<ProfileSettings>) : RecyclerView.Adapter<SettingsViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SettingsViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.list_item_settings, parent, false)

        return SettingsViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: SettingsViewHolder, position: Int) {

        if(profileSettings == null)
            return

        val settings = profileSettings[position]
        holder.title.text = settings.title

        val obj = object : SettingsViewHolder.OnViewClickListener {
            override fun onViewClick(view: View, settings: ProfileSettings?, position: Int) {
                when (position) {
                    0 -> AboutFragment.show(activity)
                    1 -> Util.sendEmail(activity, activity.getString(R.string.rate_my_plate_support_email))
                    2 -> PrivatePolicyFragment.show(activity, activity.getString(R.string.privacy))
                    3 -> TermsAndConditions.show(activity, activity.getString(R.string.terms_and_conditions))
                    4 -> ResetPasswordFragment.show(activity)
                    5 -> ModerationFragment.show(activity)
//                    6 -> Util.sendEmail(activity, activity.getString(R.string.rate_my_plate_mail))
                    6 -> RMPDialog.defaultConditionDialog(activity, activity.getString(R.string.logout_account))
                }
            }
        }

        holder.setOnViewClickListener(obj, settings, position)

    }

    override fun getItemCount(): Int {
        return profileSettings.size
    }

}