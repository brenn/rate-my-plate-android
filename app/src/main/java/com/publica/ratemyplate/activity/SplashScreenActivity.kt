package com.publica.ratemyplate.activity

import RMPManager.screen
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.publica.ratemyplate.R
import com.publica.ratemyplate.data.Preference
import com.publica.ratemyplate.ui.RMPDialog

class SplashScreenActivity : AppCompatActivity(){



    private var splashTime = 1000

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)

        val secondsDelayed = 1
        val runnable = Runnable {

            Preference.initPreference(this)
            RMPManager.code = null;
            RMPManager.screen = null;


//            val intentData = intent.data
//
//            if(intentData != null){
//                RMPManager.code = Uri.parse(intentData.toString()).getQueryParameter("data")
//                val screen = Uri.parse(intentData.toString()).getQueryParameter("screen")
//
//                RMPManager.screen = screen
//
//                if(Preference.token == ""){
//                    val intentActivity = Intent(this@SplashScreenActivity, AccountActivity::class.java)
//                    startActivity(intentActivity)
//                    finish()
//                }else{
//                    val intentActivity = Intent(this@SplashScreenActivity, MainActivity::class.java)
//                    startActivity(intentActivity)
//                    finish()
//                }
//
//            }else{

                var mainIntent: Intent? = null
                if(Preference.token == ""){

                    if(Preference.isFirstLoad){

                        mainIntent = Intent(this@SplashScreenActivity, WelcomeActivity::class.java)
                    }else{
                        mainIntent = Intent(this@SplashScreenActivity, AccountActivity::class.java)
                    }

                }else{
                    mainIntent = Intent(this@SplashScreenActivity, MainActivity::class.java)
                }

                startActivity(mainIntent)
                finish()

//            }

        }
        Handler().postDelayed(runnable, (secondsDelayed * splashTime).toLong())

    }

}