package com.publica.ratemyplate.activity

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.publica.ratemyplate.R
import com.publica.ratemyplate.data.Preference


class CodeActivity : AppCompatActivity(){

    private var splashTime = 1000

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)


        val secondsDelayed = 1
        val runnable = Runnable {

            Preference.initPreference(this)

            val intentData = intent.data

            if(intentData != null){

                RMPManager.code = Uri.parse(intentData.toString()).getQueryParameter("data")
                val screen = Uri.parse(intentData.toString()).getQueryParameter("screen")

                RMPManager.screen = screen

                if(Preference.token == ""){
                    val intentActivity = Intent(this@CodeActivity, AccountActivity::class.java)
                    startActivity(intentActivity)
                    finish()
                }else{
                    val intentActivity = Intent(this@CodeActivity, MainActivity::class.java)
                    startActivity(intentActivity)
                    finish()
                }

            }else{

                var mainIntent: Intent? = null
                if(Preference.token == ""){

                    if(Preference.isFirstLoad){
                        mainIntent = Intent(this@CodeActivity, WelcomeActivity::class.java)
                    }else{
                        mainIntent = Intent(this@CodeActivity, AccountActivity::class.java)
                    }

                }else{
                    mainIntent = Intent(this@CodeActivity, MainActivity::class.java)
                }

                startActivity(mainIntent)
                finish()

            }

        }
        Handler().postDelayed(runnable, (secondsDelayed * splashTime).toLong())

    }

    override fun onResume() {
        super.onResume()
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
    }

}