package com.publica.ratemyplate.activity

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.publica.ratemyplate.R
import com.publica.ratemyplate.fragment.BaseFragment
import com.publica.ratemyplate.fragment.CodeVerificationFragment
import com.publica.ratemyplate.fragment.PasswordCodeVerificationFragment
import com.publica.ratemyplate.fragment.RegistrationFragment
import kotlinx.android.synthetic.main.activity_account.*


class AccountActivity : AppCompatActivity(){

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_account)
        setToolBar("", View.GONE)

        back.setOnClickListener {
            onBackPressed()
        }

        if(RMPManager.code != null && RMPManager.screen == "registration"){
            CodeVerificationFragment.show(this, false)
        }else if(RMPManager.code != null && RMPManager.screen == "forget-password"){
            PasswordCodeVerificationFragment.show(this, false)
        }
        else{
            RegistrationFragment.show(this)
        }

    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        if(RMPManager.code != null && RMPManager.screen == "registration"){
            CodeVerificationFragment.show(this, false)
        }else if(RMPManager.code != null && RMPManager.screen == "forget-password"){
            PasswordCodeVerificationFragment.show(this, false)
        }
        else{
            RegistrationFragment.show(this)
        }
    }

     fun setToolBar(title: String, visibility: Int){
        toolbar_title.text = title
        toolbar.visibility = visibility
    }

    override fun onBackPressed() {
        val fragment = supportFragmentManager.findFragmentByTag(BaseFragment.CURRENT_FRAGMENT_TAG)
        if (fragment != null && fragment is BaseFragment) {
            if (!(fragment ).onBackPressed()) {
                setToolBar("", View.GONE)
                super.onBackPressed()
            }
        } else {
            super.onBackPressed()
        }

    }

}