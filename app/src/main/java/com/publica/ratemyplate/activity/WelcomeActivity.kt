package com.publica.ratemyplate.activity

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import com.publica.ratemyplate.R
import com.publica.ratemyplate.adapter.WelcomeSlideAdapter
import kotlinx.android.synthetic.main.activity_welcome.*

class WelcomeActivity : AppCompatActivity() {

    private val layouts = intArrayOf(R.layout.slide_welcome_1, R.layout.slide_welcome_2, R.layout.slide_welcome_3)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (Build.VERSION.SDK_INT >= 21) {
            window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_STABLE or
                    View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
        }

//        if (!Preference.isFirstLoad) {
//            onStartMainActivity()
//        }

        setContentView(R.layout.activity_welcome)
        setupViewPager()
    }

    private fun setupViewPager() {

        val pageAdapter = WelcomeSlideAdapter(this, layouts, object : WelcomeSlideAdapter.SlideAdapterInterface {
            override fun onButtonChanged(isLastPage: Boolean, position: Int) {
                onAddBottomDots(position)
            }

            override fun onButtonNextPage(position: Int) {
//                if (position == 4) {
//                    Preference.isFirstLoad = false
//                    onStartMainActivity()
//                } else if (position == 3) {
//                    view_pager.currentItem = position + 1
//                }

            }
        })

        view_pager.adapter = pageAdapter
        view_pager.addOnPageChangeListener(pageAdapter.viewPagerChangeListener)

        onAddBottomDots(0)
    }

    private fun onAddBottomDots(currentPage: Int) {

        val dots = arrayOfNulls<ImageView>(layouts.size)

        dots_layout.removeAllViews()

        val lp = LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        lp.setMargins(10, 0, 10, 0)

        for (i in dots.indices) {
            dots[i] = ImageView(this)

            if (currentPage == i) {
                dots[i]!!.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.paginationactive))
            } else {
                dots[i]!!.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.paginationpassive))
            }

            dots[i]!!.layoutParams = lp
            dots_layout.addView(dots[i])

        }

    }

    private fun onStartMainActivity() {
        val mainActivity = Intent(this, MainActivity::class.java)
        startActivity(mainActivity)
        finish()
    }

}