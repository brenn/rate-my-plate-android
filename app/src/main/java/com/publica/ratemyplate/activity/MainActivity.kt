package com.publica.ratemyplate.activity

import android.app.ProgressDialog
import android.content.Intent
import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v4.content.ContextCompat
import android.util.Log
import android.view.View
import com.google.gson.Gson
import com.publica.ratemyplate.R
import com.publica.ratemyplate.data.Constant
import com.publica.ratemyplate.data.Preference
import com.publica.ratemyplate.fragment.*
import com.publica.ratemyplate.model.ErrorMessage
import com.publica.ratemyplate.model.MyPlate
import com.publica.ratemyplate.network.APIClient
import com.publica.ratemyplate.network.APIInterface
import com.publica.ratemyplate.ui.RMPDialog
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Response

class MainActivity : AppCompatActivity() {

    var mListener: UpdateDataInterface? = null
    private lateinit var progressDialog: ProgressDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)



        Preference.initPreference(this)
//        requestPermissions()
        getMyPlates()

        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)

        back.setOnClickListener {
            onBackPressed()
        }

        filter_plates.setOnClickListener {
            mListener?.onShowFilterLayout()
        }

    }

    override fun onResume() {
        super.onResume()
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        getMyPlates()
    }

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->


        when (item.itemId) {
            R.id.navigation_my_plate -> {

                getMyPlates()

                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_search -> {
                removeAllStacks()
                MyPlateEmptyFragment.show(this, getString(R.string.press_search))
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_history -> {
                removeAllStacks()
                HistoryFragment.show(this)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_profile -> {
                removeAllStacks()
                ProfileFragment.show(this)
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    fun refreshToolbarState() {

        if (supportFragmentManager.backStackEntryCount == 0) {
            if (supportActionBar != null)
                supportActionBar!!.setDisplayHomeAsUpEnabled(false)
        }

        val fragment = supportFragmentManager.findFragmentByTag(BaseFragment.CURRENT_FRAGMENT_TAG)
        if (fragment != null && fragment is BaseFragment) {
            setToolbarTitle(fragment.toolbarTitle!!)
            setToolbarSubtitle(fragment.toolbarSubtitle!!)
        }

    }

    fun setToolBar(title: Int, visibility: Int) {
        toolbar.visibility = visibility
        toolbar_title.text = getString(title)
    }

    private fun setToolbarTitle(title: String) {
        toolbar_title.text = title
    }

    private fun setToolbarSubtitle(subtitle: String) {
        if (supportActionBar != null)
            supportActionBar!!.subtitle = subtitle
    }

    private fun removeAllStacks() {

        val fm = supportFragmentManager
        val count = fm.backStackEntryCount
        for (i in 0 until count) {
            supportFragmentManager.popBackStack()
        }

    }

    override fun onBackPressed() {
        val fragment = supportFragmentManager.findFragmentByTag(BaseFragment.CURRENT_FRAGMENT_TAG)
        if (fragment != null && fragment is BaseFragment) {
            if (!(fragment).onBackPressed()) {

                if (supportFragmentManager.backStackEntryCount == 1) {
                    back.visibility = View.GONE
                }

                super.onBackPressed()
            }
        } else {

            super.onBackPressed()
        }

    }

    interface UpdateDataInterface {
        fun onUpdate(filter: String)
        fun onShowFilterLayout()
    }

    fun setToolbarFilter(showFilterLayout: Boolean) {
        if (showFilterLayout) {
            filter_plates.setImageResource(R.drawable.closeicon)
            toolbar_title.visibility = View.GONE
            filter_title.visibility = View.VISIBLE
            toolbar.background = ContextCompat.getDrawable(this, R.color.filter_toolbar_bg)

        } else {
            filter_plates.setImageResource(R.drawable.editicon)
            toolbar_title.visibility = View.VISIBLE
            filter_title.visibility = View.GONE
            toolbar.setBackgroundColor(Color.TRANSPARENT);
        }
    }

    fun startLoginActivity() {
        Preference.clearPreference()
        startActivity(Intent(this, AccountActivity::class.java))
        finish()
    }

    private fun getMyPlates() {

        progressDialog = ProgressDialog.show(
            this,
            getString(R.string.please_wait),
    "",
            true
        )

        val apiInterface = APIClient.getClient(this).create(APIInterface::class.java)
        val call = apiInterface.getMyPlates(
            "plate_number.comments.comment_attachments",
            "plate_number.ratings",
            "plate_number.alerts",
            "plate_number.comments.user",
            "plate_number.comments.moderate_comment"
        )
        call.enqueue(object : retrofit2.Callback<MyPlate> {

            override fun onResponse(call: Call<MyPlate>, response: Response<MyPlate>) {
                progressDialog.dismiss()
                if (response.code() != Constant.OK_CODE) {

                    val resourceError = response.errorBody()
                    val errorMessage = Gson().fromJson(resourceError!!.string(), ErrorMessage::class.java)

                    if (errorMessage.message == getString(R.string.unauthenticated)) {
                        startLoginActivity()
                    }

                } else {

                    val resource = response.body()
                    val closeOne = resource?.closeOne
                    val others = resource?.others

                    RMPManager.family = closeOne
                    RMPManager.others = others


                    removeAllStacks()

                    if(RMPManager.code != null && RMPManager.screen == "forget-password"){
                        PasswordCodeVerificationFragment.show(this@MainActivity, false)
                    }
                    else if (closeOne?.size == 0 && others?.size == 0) {
                        MyPlateEmptyFragment.show(this@MainActivity, this@MainActivity.getString(R.string.my_plate_mes_1))
                    } else {
                        MyPlateFragment.show(this@MainActivity)
                    }

                }
            }

            override fun onFailure(call: Call<MyPlate>, t: Throwable) {
                progressDialog.dismiss()
                    RMPDialog.defaultDialog(this@MainActivity, this@MainActivity.getString(R.string.error_mes))

            }

        })

    }

//    //Camera and storage permissions
//    private fun requestPermissions() {
//        Dexter.withActivity(this)
//            .withPermissions(
//                Manifest.permission.READ_EXTERNAL_STORAGE,
//                Manifest.permission.WRITE_EXTERNAL_STORAGE,
//                Manifest.permission.ACCESS_NETWORK_STATE,
//                Manifest.permission.CAMERA
//            )
//            .withListener(object : MultiplePermissionsListener {
//                override fun onPermissionsChecked(report: MultiplePermissionsReport) {
//                    // check if all permissions are granted
//                    //                        if (report.areAllPermissionsGranted()) {
//                    //                            Toast.makeText(getApplicationContext(), "All permissions are granted!", Toast.LENGTH_SHORT).show();
//                    //                        }
//
//                    // check for permanent denial of any permission
//                    if (report.isAnyPermissionPermanentlyDenied) {
//                        // show alert dialog navigating to Settings
//                        showSettingsDialog()
//                    }
//                }
//
//                override fun onPermissionRationaleShouldBeShown(
//                    permissions: List<PermissionRequest>,
//                    token: PermissionToken
//                ) {
//                    token.continuePermissionRequest()
//                }
//            }).withErrorListener {
//                Toast.makeText(
//                    applicationContext,
//                    getString(R.string.error_occured),
//                    Toast.LENGTH_SHORT
//                ).show()
//            }
//            .onSameThread()
//            .check()
//    }

//    private fun showSettingsDialog() {
//        val builder = AlertDialog.Builder(this@MainActivity)
//        builder.setTitle(getString(R.string.need_permissions))
//        builder.setMessage(getString(R.string.permission_message))
//        builder.setPositiveButton(
//            getString(R.string.go_to_settings)
//        ) { dialog, which ->
//            dialog.cancel()
//            openSettings()
//        }
//        builder.setNegativeButton(getString(R.string.cancel), object : DialogInterface.OnClickListener {
//            override fun onClick(dialog: DialogInterface, which: Int) {
//                dialog.cancel()
//            }
//        })
//        builder.show()
//
//    }

    // navigating user to app settings
//    private fun openSettings() {
//        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
//        val uri = Uri.fromParts(Constant.PACKAGE, packageName, null)
//        intent.data = uri
//        startActivityForResult(intent, 101)
//    }

    override fun onDestroy() {
        super.onDestroy()

        if (!Preference.rememberMe)
            Preference.clearPreference()

    }



}
