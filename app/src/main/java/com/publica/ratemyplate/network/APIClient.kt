package com.publica.ratemyplate.network

import android.app.Activity
import com.publica.ratemyplate.data.Preference
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object APIClient {

    private const val KEY = "Authorization"
    private const val TIMEOUT_SEC = 60
//        private const val BASE_URL = "http://dev.ratemyplate.studiopublica.co.nz/api/"
    private const val BASE_URL = "http://ratemyplate.co.nz/api/"

    fun getClient(activity: Activity): Retrofit {

        val mInterceptor = Interceptor { chain ->
            val original = chain.request()
            val request = original.newBuilder()
                .header(KEY, Preference.token)

                .method(original.method(), original.body())
                .build()
            chain.proceed(request)
        }

        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY

        val client: OkHttpClient
        Preference.initPreference(activity.applicationContext)
        if (Preference.token.equals("")) {
            client = OkHttpClient.Builder().addInterceptor(interceptor)
                .connectTimeout(TIMEOUT_SEC.toLong(), TimeUnit.SECONDS)
                .writeTimeout(TIMEOUT_SEC.toLong(), TimeUnit.SECONDS)
                .readTimeout(TIMEOUT_SEC.toLong(), TimeUnit.SECONDS).build()
        } else {
            client = OkHttpClient.Builder().addInterceptor(mInterceptor)
                .connectTimeout(TIMEOUT_SEC.toLong(), TimeUnit.SECONDS)
                .writeTimeout(TIMEOUT_SEC.toLong(), TimeUnit.SECONDS)
                .readTimeout(TIMEOUT_SEC.toLong(), TimeUnit.SECONDS).build()
        }

        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build()

    }

}