package com.publica.ratemyplate.network

import com.publica.ratemyplate.model.*
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*

interface APIInterface {

    @Headers("Content-Type: application/json", "Accept: application/json")
    @POST("register")
    fun postRegister(@Body body: Register): Call<Register>

    @Headers("Content-Type: application/json", "Accept: application/json")
    @POST("verify")
    fun postVerify(@Body body: Verify): Call<Verify>

    @Headers("Content-Type: application/json", "Accept: application/json")
    @POST("login")
    fun postLogin(@Body body: Login): Call<Login>

    @Headers("Content-Type: application/json", "Accept: application/json")
    @POST("reset-password")
    fun postResetPassword(@Body body: Password): Call<Password>

    @Headers("Content-Type: application/json", "Accept: application/json")
    @POST("update-password")
    fun postUpdatePassword(@Body body: Verify): Call<Verify>

    @Headers("Content-Type: application/json", "Accept: application/json")
    @GET("plate-numbers?")
    fun getSearchPlate(@Query("filters[plate_number]") plate_number: String?, @Query("load[0]") ratings: String?, @Query("load[1]") comments: String?, @Query("load[2]") commentsUser: String?, @Query("load[3]") commentsModerate: String?): Call<PlateNumber>

    @Headers("Content-Type: application/json", "Accept: application/json")
    @POST("plate-numbers")
    fun postSubmitPlateNumber(@Body body: Password): Call<Password>

    @Headers("Content-Type: application/json", "Accept: application/json")
    @GET("categories")
    fun getCategories(): Call<Category>

    @Headers("Content-Type: application/json", "Accept: application/json")
    @POST("ratings")
    fun postRatings(@Body body: Rating): Call<Rating.Response>

    @Headers("Content-Type: application/json", "Accept: application/json")
    @GET("my-plates?")
    fun getMyPlates(@Query("load[0]") comments_attachment: String?, @Query("load[1]") ratings: String?, @Query("load[2]") alerts: String?, @Query("load[3]") comments_user: String?, @Query("load[4]") moderate: String?): Call<MyPlate>

    @Headers("Content-Type: application/json", "Accept: application/json")
    @POST("profile")
    fun postProfile(): Call<Profile>

    @Headers("Content-Type: application/json", "Accept: application/json")
    @POST("update-profile")
    fun postProfile(@Body body: Profile): Call<Profile>

    @Headers("Content-Type: application/json", "Accept: application/json")
    @DELETE("comment/{id}")
    fun deleteComment(@Path("id") id: Int?): Call<Comment>

    @Headers("Content-Type: application/json", "Accept: application/json")
    @POST("only-rating")
    fun postRatingOnly(@Body body: Rating): Call<Rating>

    @Headers("Content-Type: application/json", "Accept: application/json")
    @POST("comment")
    fun postCommentOnly(@Body body: Comment): Call<Comment>

    @Headers("Content-Type: application/json", "Accept: application/json")
    @POST("my-plates")
    fun postAddPlateNumber(@Body body: Rating): Call<Rating.Response>

    @Headers("Content-Type: application/json", "Accept: application/json")
    @PATCH("my-plates/{id}")
    fun postEditPlateNumber(@Path("id") id: Int?, @Body body: Rating): Call<Rating.Response>

    @Headers("Content-Type: application/json", "Accept: application/json")
    @POST("moderate-comment")
    fun postModerateComment(@Body body: Comment): Call<Moderate>

    @Headers("Content-Type: application/json", "Accept: application/json")
    @DELETE("my-plates/{id}")
    fun deletePlateOnMyList(@Path("id") id: Int?): Call<PlateNumber>

    @Headers("Content-Type: application/json", "Accept: application/json")
    @GET("history?")
    fun getHistory(@Query("load[0]") comments_attachment: String?, @Query("load[1]") ratings: String?): Call<History>

    @Headers("Content-Type: application/json", "Accept: application/json")
    @DELETE("history/{id}")
    fun deleteHistory(@Path("id") id: Int?): Call<History>

    @Headers("Content-Type: image/jpg", "Accept: application/json")
    @POST("attachments/{id}")
    fun uploadImage(@Path("id") id: Int?, @Body body: RequestBody): Call<Comment.Attachments>

    @Headers("Content-Type: application/json", "Accept: application/json")
    @POST("last-viewed-notification")
    fun updateAlert(@Body body: Alert): Call<Alert>

    @Headers("Content-Type: application/json", "Accept: application/json")
    @GET("my-plates?")
    fun getFilteredByRatingMyPlates(@Query("filters[ratings]") rating: Int?, @Query("load[0]") comments_attachment: String?, @Query("load[1]") ratings: String?, @Query("load[2]") alerts: String?, @Query("load[3]") comments_user: String?, @Query("load[4]") moderate: String?): Call<MyPlate>

    @Headers("Content-Type: application/json", "Accept: application/json")
    @GET("my-plates?")
    fun getFilteredbyCommentMyPlates(@Query("filters[comment]") rating: Int?, @Query("load[0]") comments_attachment: String?, @Query("load[1]") ratings: String?, @Query("load[2]") alerts: String?, @Query("load[3]") comments_user: String?, @Query("load[4]") moderate: String?): Call<MyPlate>

    @Headers("Content-Type: application/json", "Accept: application/json")
    @DELETE("ratings/{id}")
    fun deleteRating(@Path("id") id: Int?): Call<PlateNumber>

}