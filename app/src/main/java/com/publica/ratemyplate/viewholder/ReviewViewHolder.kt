package com.publica.ratemyplate.viewholder

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import com.daimajia.swipe.SwipeLayout
import com.publica.ratemyplate.R
import com.publica.ratemyplate.model.PlateNumber
import com.publica.ratemyplate.ui.CustomFontTextView

class ReviewViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    var swipe1: SwipeLayout
    var bottomWrapper: LinearLayout
    var delete: CustomFontTextView
    var share: CustomFontTextView
    var commentTopLayout: RelativeLayout
    var reviewer: CustomFontTextView
    var reviewDate: CustomFontTextView
    var date: CustomFontTextView
    var time: CustomFontTextView
    var category: CustomFontTextView
    var location: CustomFontTextView
    var ratingImg: ImageView
    var deleteLayout: LinearLayout


    private var onViewClickListener: OnViewClickListener? = null

    private var review: PlateNumber.Rating? = null
    private var pos: Int = 0

    init {

        swipe1 = view.findViewById(R.id.swipe_review)
        bottomWrapper = view.findViewById(R.id.bottom_wrapper)
        delete = view.findViewById(R.id.delete)
        share = view.findViewById(R.id.share)
        commentTopLayout = view.findViewById(R.id.comment_top_layout)
        reviewer = view.findViewById(R.id.reviewer_tv)
        reviewDate = view.findViewById(R.id.review_date_tv)
        date = view.findViewById(R.id.date_tv)
        time = view.findViewById(R.id.time_tv)
        category = view.findViewById(R.id.category_tv)
        location = view.findViewById(R.id.location_tv)
        ratingImg = view.findViewById(R.id.rating_img)
        deleteLayout = view.findViewById(R.id.delete_layout)

        view.setOnClickListener { view2 ->
            if (onViewClickListener != null) {
                onViewClickListener!!.onViewClick(view2, review, pos)
            }
        }

    }

    interface OnViewClickListener {
        fun onViewClick(view: View, review: PlateNumber.Rating?, position: Int)
    }

    fun setOnViewClickListener(listener: OnViewClickListener, review: PlateNumber.Rating, position: Int) {
        this.onViewClickListener = listener
        this.review = review
        this.pos = position
    }

}