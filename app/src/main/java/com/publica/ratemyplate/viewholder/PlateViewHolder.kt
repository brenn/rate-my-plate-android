package com.publica.ratemyplate.viewholder

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.LinearLayout
import com.daimajia.swipe.SwipeLayout
import com.publica.ratemyplate.R
import com.publica.ratemyplate.model.MyPlate
import com.publica.ratemyplate.model.Plate
import com.publica.ratemyplate.ui.CustomFontEditText
import com.publica.ratemyplate.ui.CustomFontTextView
import kotlinx.android.synthetic.main.list_item_my_plate.view.*

class PlateViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    var plateNumber: CustomFontTextView
    var swipe1: SwipeLayout
    var bottomWrapper: LinearLayout
    var delete: CustomFontTextView
    var edit: CustomFontTextView
    var plateName: CustomFontTextView
    var comment: CustomFontTextView
    var alert: CustomFontTextView
    var upVote: CustomFontTextView
    var downVote: CustomFontTextView
    var neutral: CustomFontTextView
    var plateNumberCardLayout: LinearLayout

    var deleteLayout: LinearLayout
    var editLayout: LinearLayout


    private var onViewClickListener: OnViewClickListener? = null

    private var plate: MyPlate.Other? = null
    private var pos: Int = 0

    init {

        plateNumber = view.findViewById(R.id.plate_number)
        swipe1 = view.findViewById(R.id.swipe_my_plate)
        bottomWrapper = view.findViewById(R.id.bottom_wrapper)
        delete = view.findViewById(R.id.delete)
        edit = view.findViewById(R.id.edit)
        plateName = view.findViewById(R.id.plate_name)
        comment = view.findViewById(R.id.comment_ctr)
        alert = view.findViewById(R.id.alert_ctr)
        upVote = view.findViewById(R.id.up_vote_ctr)
        downVote = view.findViewById(R.id.down_vote_ctr)
        neutral = view.findViewById(R.id.neutral_ctr)
        plateNumberCardLayout = view.findViewById(R.id.plate_number_card_layout)
        deleteLayout = view.findViewById(R.id.delete_layout)
        editLayout = view.findViewById(R.id.edit_layout)

        view.setOnClickListener { view2 ->
            if (onViewClickListener != null) {
                onViewClickListener!!.onViewClick(view2, plate, pos)
            }
        }

        plateNumberCardLayout.setOnClickListener { view2 ->
            if (onViewClickListener != null) {
                onViewClickListener!!.onViewClick(view2, plate, pos)
            } }

    }

    interface OnViewClickListener {
        fun onViewClick(view: View, plate: MyPlate.Other?, position: Int)
    }

    fun setOnViewClickListener(listener: OnViewClickListener, plate: MyPlate.Other, position: Int) {
        this.onViewClickListener = listener
        this.plate = plate
        this.pos = position
    }

}