package com.publica.ratemyplate.viewholder

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.RelativeLayout
import com.publica.ratemyplate.R
import com.publica.ratemyplate.model.Comment
import com.publica.ratemyplate.ui.CustomFontTextView

class CommentViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    var commentTV: CustomFontTextView
    var reviewerTV: CustomFontTextView
    var commentDateTv: CustomFontTextView
    var commentTopLayout: RelativeLayout
    var deleteModerateBtn: ImageButton
    var img1Display: ImageView
    var img2Display: ImageView
    var img3Display: ImageView


    private var onViewClickListener: OnViewClickListener? = null

    private var comment: Comment? = null
    private var pos: Int = 0

    init {

        commentTV = view.findViewById(R.id.comment_tv)
        reviewerTV = view.findViewById(R.id.reviewer_tv)
        commentDateTv = view.findViewById(R.id.comment_date_tv)
        commentTopLayout = view.findViewById(R.id.comment_top_layout)
        deleteModerateBtn = view.findViewById(R.id.delete_moderate_comment)
        img1Display = view.findViewById(R.id.img_1_display)
        img2Display = view.findViewById(R.id.img_2_display)
        img3Display = view.findViewById(R.id.img_3_display)

        view.setOnClickListener { view2 ->
            if (onViewClickListener != null) {
                onViewClickListener!!.onViewClick(view2, comment, pos)
            }
        }

    }

    interface OnViewClickListener {
        fun onViewClick(view: View, comment: Comment?, position: Int)
    }

    fun setOnViewClickListener(listener: OnViewClickListener, comment: Comment, position: Int) {
        this.onViewClickListener = listener
        this.comment = comment
        this.pos = position
    }

}