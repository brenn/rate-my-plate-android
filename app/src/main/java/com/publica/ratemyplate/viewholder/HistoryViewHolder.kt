package com.publica.ratemyplate.viewholder

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.daimajia.swipe.SwipeLayout
import com.publica.ratemyplate.R
import com.publica.ratemyplate.R.string.settings
import com.publica.ratemyplate.model.History
import com.publica.ratemyplate.ui.CustomFontEditText
import com.publica.ratemyplate.ui.CustomFontTextView

class HistoryViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    var plateNumber: CustomFontTextView
    var swipe1: SwipeLayout
    var bottomWrapper: LinearLayout
    var delete: LinearLayout
    var actionImg: ImageView
    var swipeCard: LinearLayout

    private var onViewClickListener: OnViewClickListener? = null

    private var history: History.Datum? = null
    private var pos: Int = 0

    init {

        plateNumber = view.findViewById(R.id.plate_number)
        swipe1 = view.findViewById(R.id.swipe_history)
        bottomWrapper = view.findViewById(R.id.bottom_wrapper)
        delete = view.findViewById(R.id.delete)
        actionImg = view.findViewById(R.id.action)
        swipeCard = view.findViewById(R.id.swipe_card)

        view.setOnClickListener { view2 ->
            if (onViewClickListener != null) {
                onViewClickListener!!.onViewClick(view2, history, pos)
            }
        }

    }

    interface OnViewClickListener {
        fun onViewClick(view: View, history: History.Datum?, position: Int)
    }

    fun setOnViewClickListener(listener: OnViewClickListener, history: History.Datum, position: Int) {
        this.onViewClickListener = listener
        this.history = history
        this.pos = position
    }

}