package com.publica.ratemyplate.viewholder

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import com.publica.ratemyplate.R
import com.publica.ratemyplate.model.ProfileSettings

class SettingsViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    var title: TextView

    private var onViewClickListener: OnViewClickListener? = null

    private var isSettings: ProfileSettings? = null
    private var pos: Int = 0

    init {

        title = view.findViewById(R.id.name)

        view.setOnClickListener { view2 ->
            if (onViewClickListener != null) {
                onViewClickListener!!.onViewClick(view2, isSettings, pos)
            }
        }

    }

    interface OnViewClickListener {
        fun onViewClick(view: View, settings: ProfileSettings?, position: Int)
    }

    fun setOnViewClickListener(listener: OnViewClickListener, settings: ProfileSettings, position: Int) {
        this.onViewClickListener = listener
        this.isSettings = settings
        this.pos = position
    }

}
