package com.publica.ratemyplate.ui

import android.content.Context
import android.graphics.Typeface
import java.util.HashMap

object FontCache {
    private const val EXTENSION_FILE = ".ttf"
    private const val  FONTS = "fonts/"
    private val fontMap = HashMap<String, Typeface>()

    fun getTypeface(fontName: String, context: Context): Typeface? {
        var fontName2 = fontName

        fontName2 += EXTENSION_FILE

        if (fontMap.containsKey(fontName2)) {
            return fontMap[fontName2]
        } else {
            val tf = Typeface.createFromAsset(context.assets, FONTS + fontName2)
            fontMap[fontName2] = tf
            return tf
        }
    }

}