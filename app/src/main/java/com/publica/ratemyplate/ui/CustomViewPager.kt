package com.publica.ratemyplate.ui

import android.content.Context
import android.text.method.Touch.onTouchEvent
import android.view.MotionEvent
import android.support.v4.view.ViewPager
import android.util.AttributeSet


class CustomViewPager : ViewPager {

    private var enableSwipe: Boolean = false

    constructor(context: Context) : super(context) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init()
    }

    private fun init() {
        enableSwipe = true
    }

    override fun onInterceptTouchEvent(event: MotionEvent): Boolean {
        // Never allow swiping to switch between pages
        return enableSwipe && super.onInterceptTouchEvent(event)
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        // Never allow swiping to switch between pages
        return enableSwipe && super.onTouchEvent(event)

    }

    fun setEnableSwipe(enableSwipe: Boolean) {
        this.enableSwipe = enableSwipe
    }
}