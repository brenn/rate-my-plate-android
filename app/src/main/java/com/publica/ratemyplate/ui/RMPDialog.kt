package com.publica.ratemyplate.ui

import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.support.v4.app.FragmentActivity
import com.publica.ratemyplate.R
import com.publica.ratemyplate.activity.AccountActivity
import com.publica.ratemyplate.data.Preference
import kotlinx.android.synthetic.main.dialog_message.view.*


object RMPDialog {


    fun defaultDialog(activity: FragmentActivity, message: String) {

        val builder = AlertDialog.Builder(activity)
        builder.setPositiveButton(activity.getString(R.string.ok), DialogInterface.OnClickListener { _, _ ->

        })

        val inflater = activity.layoutInflater
        val view = inflater.inflate(R.layout.dialog_message, null)
        view.dialog_mess.text = message
        val dialog = builder.create()
        dialog.setView(view)
        dialog.show()

    }

    fun requestModerationDialog(activity: FragmentActivity, message: String) {

        val builder = AlertDialog.Builder(activity)
        builder.setPositiveButton(activity.getString(R.string.ok), DialogInterface.OnClickListener { _, _ ->

        })

        val inflater = activity.layoutInflater
        val view = inflater.inflate(R.layout.dialog_request_moderation, null)
        view.dialog_mess.text = message
        val dialog = builder.create()
        dialog.setView(view)
        dialog.show()

    }

    fun defaultConditionDialog(activity: FragmentActivity, message: String) {

        val builder = AlertDialog.Builder(activity)
        builder.setPositiveButton(
            activity.getString(R.string.logout),
            DialogInterface.OnClickListener { _, _ ->

                Preference.clearPreference()
                activity.startActivity(Intent(activity, AccountActivity::class.java))
                activity.finish()

            })

        builder.setNegativeButton(activity.getString(R.string.cancel), null)

        val inflater = activity.layoutInflater
        val view = inflater.inflate(R.layout.dialog_message, null)
        view.dialog_mess.text = message
        val dialog = builder.create()
        dialog.setView(view)
        dialog.show()

    }

//    Rating



}