package com.publica.ratemyplate.ui

import android.content.Context
import android.graphics.Typeface
import android.support.v7.widget.AppCompatEditText
import android.util.AttributeSet
import com.publica.ratemyplate.R

class CustomFontEditText : AppCompatEditText {

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {

        applyCustomFont(context, attrs)
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {

        applyCustomFont(context, attrs)
    }

    private fun applyCustomFont(context: Context, attrs: AttributeSet) {
        val attributeArray = context.obtainStyledAttributes(
            attrs,
            R.styleable.CustomFontEditText)

        val fontName = attributeArray.getString(R.styleable.CustomFontEditText_custom_field_font)
        val textStyle = attrs.getAttributeIntValue(ANDROID_SCHEMA, "textStyle", Typeface.NORMAL)

        val customFont = selectTypeface(context, fontName!!, textStyle)
        typeface = customFont

        attributeArray.recycle()
    }

    private fun selectTypeface(context: Context, fontName: String, textStyle: Int): Typeface? {

        val bold = context.getString(R.string.sf_pro_bold)
        val semiBold = context.getString(R.string.sf_pro_semi_bold)
        val medium = context.getString(R.string.sf_pro_medium)
        val light = context.getString(R.string.sf_pro_light)
        val regular = context.getString(R.string.sf_pro_regular)


        return if (fontName.contentEquals(bold)) {
            FontCache.getTypeface(bold, context)
        } else if (fontName.contentEquals(medium)) {
            FontCache.getTypeface(medium, context)
        } else if (fontName.contentEquals(light)) {
            FontCache.getTypeface(light, context)
        } else if (fontName.contentEquals(regular)) {
            FontCache.getTypeface(regular, context)
        } else if (fontName.contentEquals(semiBold)) {
            FontCache.getTypeface(semiBold, context)
        } else {
            // no matching font found
            // return null so Android just uses the standard font (Roboto)
            null
        }

    }

    companion object {

        const val ANDROID_SCHEMA = "http://schemas.android.com/apk/res/android"

    }

}