package com.publica.ratemyplate.fragment

import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.media.ThumbnailUtils
import android.os.Bundle
import android.support.v4.app.FragmentActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.*
import com.publica.ratemyplate.R
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_rate_plate.view.*
import com.google.gson.Gson
import com.publica.ratemyplate.activity.MainActivity
import com.publica.ratemyplate.data.Constant
import com.publica.ratemyplate.data.Preference
import com.publica.ratemyplate.model.Comment
import com.publica.ratemyplate.model.ErrorMessage
import com.publica.ratemyplate.model.Rating
import com.publica.ratemyplate.network.APIClient
import com.publica.ratemyplate.network.APIInterface
import com.publica.ratemyplate.ui.RMPDialog
import com.publica.ratemyplate.util.Util
import kotlinx.android.synthetic.main.dialog_date_time.view.*
import kotlinx.android.synthetic.main.dialog_message.view.*
import okhttp3.MediaType
import okhttp3.RequestBody
import pl.aprilapps.easyphotopicker.DefaultCallback
import pl.aprilapps.easyphotopicker.EasyImage
import retrofit2.Call
import retrofit2.Response
import java.io.ByteArrayOutputStream
import java.io.File
import java.util.*
import kotlin.math.min


class RatePlateFragment : BaseFragment() {

    private lateinit var mainView: View
    private lateinit var mainActivity: MainActivity

    var addToMyPlateToggle: Boolean = false
    var familyToggle: Boolean = true
    var othersToggle: Boolean = false
    var rating: Int = -10;

    private lateinit var progressDialog: ProgressDialog

    companion object {
        fun show(activity: FragmentActivity) {
            val fragment = RatePlateFragment()
            fragment.push(activity, activity.getString(R.string.rate_a_plate), "")

        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(R.layout.fragment_rate_plate, container, false)


        mainView = view
        mainActivity = activity as MainActivity


        val imm = context!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(mainView.windowToken, 0)

        if(RMPManager.searchPlateNumber == null) {
            RMPDialog.defaultDialog(mainActivity, getString(R.string.error_mes))
            return null
        }

        val calendar = Calendar.getInstance()

        val year = calendar.get(Calendar.YEAR)
        val month = calendar.get(Calendar.MONTH)
        val day = calendar.get(Calendar.DAY_OF_MONTH)
        val hour = calendar.get(Calendar.HOUR_OF_DAY)
        val minute = calendar.get(Calendar.MINUTE)

        var stringMin = ""
        if(minute < 10){
            stringMin = "0" + minute
        }else {
            stringMin = "" + minute
        }

        mainView.plate_number_tv.text = RMPManager.searchPlateNumber
        mainView.date_time_tv.text = String.format(getString(R.string.date_time_format, day, month, year, hour, stringMin))


        val categories = mainActivity.resources.getStringArray(R.array.main_categories)

            val adapter = ArrayAdapter(mainActivity, android.R.layout.simple_spinner_item, categories)
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            mainView.categories_spinner.adapter = adapter

        mainView.categories_spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                var subCategories = mainActivity.resources.getStringArray(R.array.driving);

                when (position) {
                    1 -> subCategories = mainActivity.resources.getStringArray(R.array.parking);
                    2 -> subCategories = mainActivity.resources.getStringArray(R.array.vehicle_maintenance);
                    3 -> subCategories = mainActivity.resources.getStringArray(R.array.security);
                }


                val subAdapter = ArrayAdapter(mainActivity, android.R.layout.simple_spinner_item, subCategories)
                subAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                mainView.sub_categories_spinner.adapter = subAdapter

            }

        }

        mainActivity.navigation.itemBackgroundResource = R.drawable.footermainmenulight


        mainView.add_to_my_plates_toggle.setOnClickListener {
            if (!addToMyPlateToggle) {
                addToMyPlateToggle = true
                mainView.add_to_my_plates_toggle.setImageResource(R.drawable.selected)
                mainView.add_to_my_plate_layout.visibility = View.VISIBLE
            } else {
                addToMyPlateToggle = false
                mainView.add_to_my_plates_toggle.setImageResource(R.drawable.notselected)
                mainView.add_to_my_plate_layout.visibility = View.GONE
            }
        }

        mainView.family_toggle.setOnClickListener {
            if (!familyToggle) {
                familyToggle = true
                othersToggle = false
                mainView.family_toggle.setImageResource(R.drawable.selected)
                mainView.others_toggle.setImageResource(R.drawable.notselected)
            } else {
                familyToggle = false
                othersToggle = true
                mainView.family_toggle.setImageResource(R.drawable.notselected)
                mainView.others_toggle.setImageResource(R.drawable.selected)
            }
        }

        mainView.others_toggle.setOnClickListener {
            if (!othersToggle) {
                familyToggle = false
                othersToggle = true
                mainView.family_toggle.setImageResource(R.drawable.notselected)
                mainView.others_toggle.setImageResource(R.drawable.selected)
            } else {
                familyToggle = true
                othersToggle = false
                mainView.family_toggle.setImageResource(R.drawable.selected)
                mainView.others_toggle.setImageResource(R.drawable.notselected)
            }
        }

        mainView.like_btn.setOnClickListener {
            rating = Constant.LIKE
            mainView.like_btn.setImageResource(R.drawable.ratepositiveicon)
            mainView.dislike_btn.setImageResource(R.drawable.ratednegativeicon_2)
            mainView.neutral_btn.setImageResource(R.drawable.ratedneutralicon_2)
        }


        mainView.neutral_btn.setOnClickListener {
            rating = Constant.NEUTRAL
            mainView.like_btn.setImageResource(R.drawable.ratedpositiveicon_2)
            mainView.dislike_btn.setImageResource(R.drawable.ratednegativeicon_2)
            mainView.neutral_btn.setImageResource(R.drawable.rateneutralicon)
        }

        mainView.dislike_btn.setOnClickListener {
            rating = Constant.DISLIKE
            mainView.like_btn.setImageResource(R.drawable.ratedpositiveicon_2)
            mainView.dislike_btn.setImageResource(R.drawable.ratenegativeicon)
            mainView.neutral_btn.setImageResource(R.drawable.ratedneutralicon_2)
        }

        mainView.date_time_tv.setOnClickListener {
            showDateAndTimeDialog(mainView)
        }

        mainView.submit_btn.setOnClickListener {

            var validationMes = ""

            if (mainView.date_time_tv.text.toString().contains(getString(R.string.date)) || mainView.date_time_tv.text.toString().contains(getString(R.string.time)) || mainView.date_time_tv.text.toString() == "") {
                validationMes = validationMes + getString(R.string.val_date_time)
            }

            if (mainView.location_et.text.toString() == "") {
                validationMes = validationMes + getString(R.string.val_location)
            }

            if(rating == -10){
                validationMes = validationMes + getString(R.string.val_rate_icon)
            }

            if(validationMes != ""){
                RMPDialog.defaultDialog(mainActivity, validationMes)
                return@setOnClickListener
            }

            val plateNumber = RMPManager.searchPlateNumber!!
            val category = mainView.sub_categories_spinner.selectedItem.toString()
            val comment = ""
            val timing = Util.dateToEpoch(mainView.date_time_tv.text.toString(), false)
            val location = mainView.location_et.text.toString()
            var addToMyPlates = 1
            val plateName = mainView.plate_name_et.text.toString()
            var plateType = 1

            if (!familyToggle) {
                plateType = 0
            }

            if (!addToMyPlateToggle) {
                addToMyPlates = 0
            }

//            comment == "" ||
//            if ( mainView.date_time_tv.text.toString() == "" || location == "") {
//                RMPDialog.defaultDialog(mainActivity, getString(R.string.please_fillup_form))
//                return@setOnClickListener
//            }
            mainView.submit_btn.isEnabled = false
            progressDialog = ProgressDialog.show(
                mainActivity,
                getString(R.string.please_wait),
                getString(R.string.submitting_plate_number),
                true
            )

            val apiInterface = APIClient.getClient(mainActivity).create(APIInterface::class.java)
            val call = apiInterface.postRatings(
                Rating(plateNumber, rating, category, timing, location, comment, addToMyPlates, plateName, plateType)
            )
            call.enqueue(object : retrofit2.Callback<Rating.Response> {

                override fun onResponse(call: Call<Rating.Response>, response: Response<Rating.Response>) {
                    mainView.submit_btn.isEnabled = true

                    if (response.code() != Constant.OK_CODE) {

                        val resourceError = response.errorBody()
                        val errorMessage = Gson().fromJson(resourceError!!.string(), ErrorMessage::class.java)

                        if (errorMessage.message == mainActivity.getString(R.string.unauthenticated)) {
                            mainActivity.startLoginActivity()
                        } else {
                            progressDialog.dismiss()
                            RMPDialog.defaultDialog(mainActivity, errorMessage!!.message!!)
                        }

                    } else {

//                        val resource = response.body()

//                        val data = resource!!.data!!

//
                        progressDialog.dismiss()
                        defaultRatingDialog(
                            mainActivity,
                            mainActivity.getString(R.string.thank_you_for_rating_this_plate)
                        )

                    }
                }

                override fun onFailure(call: Call<Rating.Response>, t: Throwable) {
                   if(isAdded){
                       mainView.submit_btn.isEnabled = true
                       progressDialog.dismiss()
                       RMPDialog.defaultDialog(mainActivity, getString(R.string.error_mes))
                   }
                }

            })

        }

        return view
    }

    fun defaultRatingDialog(activity: FragmentActivity, message: String) {

        val builder = AlertDialog.Builder(activity)
        builder.setPositiveButton(activity.getString(R.string.ok), DialogInterface.OnClickListener { _, _ ->
            if(Preference.firstRate){
                defaultRatingSecondDialog(
                    mainActivity,
                    mainActivity.getString(R.string.submitted_rate_second_mes)
                )
                Preference.firstRate = false
            }else{
                mainActivity.navigation.selectedItemId = R.id.navigation_my_plate
            }

        })

        val inflater = activity.layoutInflater
        val view = inflater.inflate(R.layout.dialog_message, null)
        view.dialog_mess.text = message
        val dialog = builder.create()
        dialog.setView(view)
        dialog.show()

    }

    fun defaultRatingSecondDialog(activity: FragmentActivity, message: String) {

        val builder = AlertDialog.Builder(activity)
        builder.setPositiveButton(activity.getString(R.string.ok), DialogInterface.OnClickListener { _, _ ->
            mainActivity.navigation.selectedItemId = R.id.navigation_my_plate
        })

        val inflater = activity.layoutInflater
        val view = inflater.inflate(R.layout.dialog_message, null)
        view.dialog_mess.text = message
        val dialog = builder.create()
        dialog.setView(view)
        dialog.show()

    }

    override fun onDestroy() {
        super.onDestroy()
        mainActivity.navigation.itemBackgroundResource = R.drawable.footermainmenu
    }

    private fun showDateAndTimeDialog(mainView: View) {

        val dateTime = mainView.date_time_tv.text.split(" ")


        val builder = AlertDialog.Builder(mainActivity)

        val inflater = LayoutInflater.from(mainActivity)
        val view = inflater.inflate(R.layout.dialog_date_time, null)



        view.date.text = dateTime[0]
        view.time.text = dateTime[1]
//        view.date.text = String.format(mainActivity.getString(R.string.date_format, day, month, year))
//        view.time.text = String.format(mainActivity.getString(R.string.time_format, hour, minute))

        view.date.setOnClickListener {
            Util.onDatePickerClick(view.date, mainActivity)
        }

        view.time.setOnClickListener {
            Util.onTimePickerClick(view.time, mainActivity)
        }

        builder.setView(view)
        builder.setPositiveButton(
            activity?.getString(R.string.ok),
            DialogInterface.OnClickListener { _, _ ->
                mainView.date_time_tv.text = view.date.text.toString() + " " + view.time.text.toString()
            })

        builder.setNegativeButton(activity?.getString(R.string.cancel), null)

        val dialog = builder.create()
        dialog.show()

    }

}