package com.publica.ratemyplate.fragment

import android.content.Context
import android.os.Bundle
import android.support.v4.app.FragmentActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import com.publica.ratemyplate.R
import com.publica.ratemyplate.activity.MainActivity
import com.publica.ratemyplate.ui.RMPDialog
import com.publica.ratemyplate.util.Util
import kotlinx.android.synthetic.main.fragment_about_app.view.*
import kotlinx.android.synthetic.main.fragment_search_plate.view.*

class AboutFragment: BaseFragment(){

    private lateinit var mainActivity: MainActivity

    companion object {
        fun show(activity: FragmentActivity) {
            val fragment = AboutFragment()
            fragment.push(activity, activity.getString(R.string.about_app), "")
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(R.layout.fragment_about_app, container, false)
        mainActivity = activity as MainActivity
        mainActivity.setToolBar(R.string.about_app, View.VISIBLE)

        view.info_email.setOnClickListener {

            Util.sendEmail(mainActivity, mainActivity.getString(R.string.about_the_app_message_3))

        }

        return view
    }

    override fun onDestroyView() {
        super.onDestroyView()
        mainActivity.setToolBar(R.string.nav_profile, View.VISIBLE)

    }

}