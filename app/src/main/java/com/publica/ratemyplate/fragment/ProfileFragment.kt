package com.publica.ratemyplate.fragment

import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.FragmentActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.gson.Gson
import com.publica.ratemyplate.R
import com.publica.ratemyplate.activity.MainActivity
import com.publica.ratemyplate.adapter.SettingsAdapter
import com.publica.ratemyplate.data.Constant
import com.publica.ratemyplate.model.ErrorMessage
import com.publica.ratemyplate.model.Profile
import com.publica.ratemyplate.model.ProfileSettings
import com.publica.ratemyplate.network.APIClient
import com.publica.ratemyplate.network.APIInterface
import com.publica.ratemyplate.ui.RMPDialog
import com.publica.ratemyplate.util.Util
import kotlinx.android.synthetic.main.fragment_profile.view.*
import retrofit2.Call
import retrofit2.Response
import java.util.ArrayList
import android.graphics.Typeface
import android.view.inputmethod.InputMethodManager
import com.publica.ratemyplate.activity.AccountActivity
import com.publica.ratemyplate.data.Constant.Companion.FEMALE
import com.publica.ratemyplate.data.Constant.Companion.MALE
import com.publica.ratemyplate.data.Constant.Companion.OTHER
import com.publica.ratemyplate.data.Preference
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.dialog_message.view.*
import android.graphics.PorterDuff
import android.graphics.PorterDuffColorFilter
import android.support.v4.content.ContextCompat
import android.widget.Toast


class ProfileFragment : BaseFragment() {

    private lateinit var mainActivity: MainActivity
    private lateinit var mainView: View
    private lateinit var profile: Profile
    private lateinit var progressDialog: ProgressDialog

    companion object {
        fun show(activity: FragmentActivity) {
            val fragment = ProfileFragment()
            fragment.clearPush(activity, activity.getString(com.publica.ratemyplate.R.string.nav_profile), "")

        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(com.publica.ratemyplate.R.layout.fragment_profile, container, false)
        mainActivity = activity as MainActivity
        mainActivity.toolbar_title.text = getString(com.publica.ratemyplate.R.string.nav_profile)
        mainView = view

        val imm = context!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(mainView.windowToken, 0)

        val font = Typeface.createFromAsset(mainActivity.assets, "fonts/SF-Pro-Text-Semibold.ttf")
        mainView.radio_male.typeface = font
        mainView.radio_female.typeface = font
        mainView.radio_other.typeface = font

        val drawable = ContextCompat.getDrawable(mainActivity, com.publica.ratemyplate.R.drawable.editicon_profile)
        drawable!!.setColorFilter(ContextCompat.getColor(mainActivity, R.color.rmp_blue), PorterDuff.Mode.SRC_IN);

        val settingsNames = mainActivity.resources.getStringArray(com.publica.ratemyplate.R.array.my_profile_settings)
        val items = ArrayList<ProfileSettings>()

        for (name in settingsNames) {
            items.add(ProfileSettings(name))
        }

        val layoutManager = LinearLayoutManager(activity)
        val dividerItemDecoration = DividerItemDecoration(
            mainView.settings_recycler_view.getContext(),
            layoutManager.orientation
        )
        mainView.settings_recycler_view.addItemDecoration(dividerItemDecoration)
        mainView.settings_recycler_view.layoutManager = layoutManager
        val adapter = SettingsAdapter(activity as FragmentActivity, items)
        mainView.settings_recycler_view.adapter = adapter

        getProfile()
        setFieldsFocus(false)

        mainView.date_of_birth_et.setOnClickListener {
            Util.onDatePickerClick(mainView.date_of_birth_et, mainActivity)
        }

        mainView.edit_profile_btn.setOnClickListener {
            Toast.makeText(mainActivity, "Edit profile mode", Toast.LENGTH_LONG).show()
            setFieldsFocus(true)
            mainView.edit_profile_btn.visibility = View.INVISIBLE
            mainView.buttons_layout.visibility = View.GONE
            mainView.save_cancel_layout.visibility = View.VISIBLE
        }

        mainView.delete_account_btn.setOnClickListener {
            deleteAccountDialog()
        }

        mainView.cancel_profile.setOnClickListener {
            defaultView()
            setFieldsFocus(false)
        }

        mainView.save_profile.setOnClickListener {

            val email = mainView.email_et.text.toString()
            val nickName = mainView.nickname_et.text.toString()
            val firstName = mainView.first_name_et.text.toString()
            val lastName = mainView.last_name_et.text.toString()
            val city = mainView.city_et.text.toString()

//            if (firstName == "" || lastName == "" || nickName == "" || city == "" || email == "") {
//
//                RMPDialog.defaultDialog(mainActivity, getString(R.string.please_fillup_form))
//                return@setOnClickListener
//
//            }

            var gender = MALE
            if (mainView.radio_sex.checkedRadioButtonId == com.publica.ratemyplate.R.id.radio_female){
                gender = FEMALE
            }else if(mainView.radio_sex.checkedRadioButtonId == com.publica.ratemyplate.R.id.radio_other){
                gender = OTHER
            }

            mainView.save_profile.isEnabled = false
            progressDialog = ProgressDialog.show(mainActivity, getString(com.publica.ratemyplate.R.string.please_wait), getString(
                com.publica.ratemyplate.R.string.updating_profile), true)
            val apiInterface = APIClient.getClient(mainActivity).create(APIInterface::class.java)
            val call = apiInterface.postProfile(
                Profile(
                    email,
                    nickName,
                    firstName,
                    lastName,
                    gender,
                    Util.dateToEpoch(mainView.date_of_birth_et.text.toString(), true).toInt() ,
                    city
                )

            )
            call.enqueue(object : retrofit2.Callback<Profile> {

                override fun onResponse(call: Call<Profile>, response: Response<Profile>) {
                    progressDialog.dismiss()
                    mainView.save_profile.isEnabled = true
                    if (response.code() != Constant.OK_CODE) {
//                        defaultView()
                        val resourceError = response.errorBody()
                        val errorMessage = Gson().fromJson(resourceError!!.string(), ErrorMessage::class.java)

                        if(errorMessage.message == "The given data was invalid."){

                            var errorMes = ""

                            if(errorMessage.errors!!.email != null){
                                errorMes = errorMes + errorMessage.errors!!.email!!.first() + "\n"
                            }

                            if(errorMessage.errors!!.nickname != null){
                                errorMes = errorMes + errorMessage.errors!!.nickname!!.first() + "\n"
                            }

                            if(errorMessage.errors!!.first_name != null){
                                errorMes = errorMes + errorMessage.errors!!.first_name!!.first() + "\n"
                            }

                            if(errorMessage.errors!!.last_name != null){
                                errorMes = errorMes + errorMessage.errors!!.last_name!!.first() + "\n"
                            }

                            if(errorMessage.errors!!.age != null){
                                errorMes = errorMes + errorMessage.errors!!.age!!.first() + "\n"
                            }

                            if(errorMessage.errors!!.gender != null){
                                errorMes = errorMes + errorMessage.errors!!.gender!!.first() + "\n"
                            }

                            if(errorMessage.errors!!.city != null){
                                errorMes = errorMes + errorMessage.errors!!.city!!.first() + "\n"
                            }

                            RMPDialog.defaultDialog(mainActivity, errorMes)

                        }else if (errorMessage.message == mainActivity.getString(com.publica.ratemyplate.R.string.unauthenticated)) {

                            mainActivity.startLoginActivity()
                        }
                    } else {

                        val resource = response.body()!!
                        val message = resource.message!!

                        setProfileModel(email, nickName, firstName, lastName)

                        RMPDialog.defaultDialog(mainActivity, message)

                        mainView.buttons_layout.visibility = View.VISIBLE
                        mainView.save_cancel_layout.visibility = View.GONE

                    }

                    setFieldsFocus(false);
                }

                override fun onFailure(call: Call<Profile>, t: Throwable) {
                    if(isAdded){
                        mainView.save_profile.isEnabled = true
                        progressDialog.dismiss()
                        RMPDialog.defaultDialog(mainActivity, getString(com.publica.ratemyplate.R.string.error_mes))
                    }
                }

            })

        }

        return view
    }


    private fun getProfile() {

        val apiInterface = APIClient.getClient(mainActivity).create(APIInterface::class.java)
        val call = apiInterface.postProfile()
        call.enqueue(object : retrofit2.Callback<Profile> {

            override fun onResponse(call: Call<Profile>, response: Response<Profile>) {

                if (response.code() != Constant.OK_CODE) {

                    val resourceError = response.errorBody()
                    val errorMessage = Gson().fromJson(resourceError!!.string(), ErrorMessage::class.java)

                    if (errorMessage.message == getString(com.publica.ratemyplate.R.string.unauthenticated)) {

                        mainActivity.startLoginActivity()
                    }
                } else {

                    val resource = response.body()!!
                    profile = resource

                    val email = profile.email!!
                    val username = profile.nickname
                    val firstname = profile.firstName
                    val lastname = profile.lastName

                    mainView.first_name_tv.text = username
                    mainView.registered_year_tv.text = "Registered since " + profile.createdYear
                    mainView.first_name_et.setText(firstname)
                    mainView.last_name_et.setText(lastname)
                    mainView.nickname_et.setText(username)
                    mainView.date_of_birth_et.text = Util.epochToDate(profile.age!!.toLong())
                    mainView.city_et.setText(profile.city)
                    mainView.email_et.setText(profile.email)
                    Preference.email = email

                    setProfileModel(email, username!!, firstname!!, lastname!!)

                    if (profile.gender == FEMALE) {
                        mainView.radio_sex.check(com.publica.ratemyplate.R.id.radio_female)
                    } else if(profile.gender == MALE) {
                        mainView.radio_sex.check(com.publica.ratemyplate.R.id.radio_male)
                    }else{
                        mainView.radio_sex.check(com.publica.ratemyplate.R.id.radio_other)
                    }
                }


            }

            override fun onFailure(call: Call<Profile>, t: Throwable) {
                if(isAdded){
                    RMPDialog.defaultDialog(mainActivity, getString(R.string.error_mes))
                }
            }

        })

    }

    private fun setFieldsFocus(focus: Boolean) {
        mainView.first_name_et.isFocusable = focus
        mainView.last_name_et.isFocusable = focus
        mainView.nickname_et.isFocusable = false
        mainView.date_of_birth_et.isEnabled = focus
        mainView.radio_male.isClickable = focus
        mainView.radio_female.isClickable = focus
        mainView.radio_other.isClickable = focus
        mainView.city_et.isFocusable = focus
        mainView.email_et.isFocusable = focus

        mainView.first_name_et.isFocusableInTouchMode = focus
        mainView.last_name_et.isFocusableInTouchMode = focus
        mainView.nickname_et.isFocusableInTouchMode = false
        mainView.date_of_birth_et.isFocusableInTouchMode = focus
        mainView.city_et.isFocusableInTouchMode = focus
        mainView.email_et.isFocusableInTouchMode = focus

    }

    private fun defaultView(){

        if(profile == null){
            return
        }

        mainView.first_name_tv.text = profile.nickname
        mainView.registered_year_tv.text = "Registered since " + profile.createdYear
        mainView.first_name_et.setText(profile.firstName)
        mainView.last_name_et.setText(profile.lastName)
        mainView.nickname_et.setText(profile.nickname)
        mainView.date_of_birth_et.text = Util.epochToDate(profile.age!!.toLong())
        mainView.city_et.setText(profile.city)
        mainView.email_et.setText(profile.email)

        if (profile.gender == FEMALE) {
            mainView.radio_sex.check(com.publica.ratemyplate.R.id.radio_female)
        } else if(profile.gender == MALE) {
            mainView.radio_sex.check(com.publica.ratemyplate.R.id.radio_male)
        }else {
            mainView.radio_sex.check(com.publica.ratemyplate.R.id.radio_other)
        }

        mainView.edit_profile_btn.visibility = View.VISIBLE

//        mainView.buttons_layout.visibility = View.VISIBLE
        mainView.save_cancel_layout.visibility = View.GONE
    }

    private fun deleteAccountDialog(){

        val builder = AlertDialog.Builder(mainActivity)
        builder.setPositiveButton(
            mainActivity.getString(com.publica.ratemyplate.R.string.delete) ,
            DialogInterface.OnClickListener { dialog, which ->

                //api here

                Preference.clearPreference()
                mainActivity.startActivity(Intent(mainActivity, AccountActivity::class.java))
                mainActivity.finish()

            })

        builder.setNegativeButton(mainActivity.getString(com.publica.ratemyplate.R.string.cancel), null)

        val inflater = mainActivity.layoutInflater
        val view = inflater.inflate(com.publica.ratemyplate.R.layout.dialog_message, null)

        builder.setTitle(getString(com.publica.ratemyplate.R.string.delete_account_confirmation))
        view.dialog_mess.text = getString(com.publica.ratemyplate.R.string.delete_account_confirmation_mes)

        val dialog = builder.create()
        dialog.setView(view)
        dialog.show()

    }

    private fun setProfileModel(email: String, username: String, firstname: String, lastname: String){

        RMPManager.profile = Profile(email, username, firstname, lastname, "", 0, "")

    }
}