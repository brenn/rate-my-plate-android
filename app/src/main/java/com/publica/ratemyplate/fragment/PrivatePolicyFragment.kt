package com.publica.ratemyplate.fragment

import android.content.Context
import android.os.Bundle
import android.support.v4.app.FragmentActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import com.publica.ratemyplate.R
import com.publica.ratemyplate.activity.AccountActivity
import com.publica.ratemyplate.activity.MainActivity
import com.publica.ratemyplate.ui.CustomWebViewClient
import kotlinx.android.synthetic.main.fragment_terms_and_conditions.view.*

class PrivatePolicyFragment : BaseFragment(){


    companion object {


        fun show(activity: FragmentActivity, title: String) {
            val fragment = PrivatePolicyFragment()
            fragment.push(activity, title, "")

        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(com.publica.ratemyplate.R.layout.fragment_private_policy, container, false)

        val imm = context!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)

        if(activity is AccountActivity){
            val act = activity as AccountActivity
            act.setToolBar(getString(com.publica.ratemyplate.R.string.terms_and_conditions), View.VISIBLE)
        }

//        view.web_view.webViewClient = CustomWebViewClient()
//        view.web_view.settings.loadsImagesAutomatically = true;
//        view.web_view.settings.javaScriptEnabled = true;
//        view.web_view.scrollBarStyle = View.SCROLLBARS_INSIDE_OVERLAY;
//        view.web_view.loadUrl("https://drive.google.com/file/d/13fagjHp0Siht3k4kGkBTe5vJU_-0CxEf/view?usp=sharing");

//        "https//drive.google.com/file/d/13fagjHp0Siht3k4kGkBTe5vJU_-0CxEf/view?usp=sharing"

        return view
    }


    override fun onDestroyView() {
        super.onDestroyView()

        if(activity is AccountActivity){
            val act = activity as AccountActivity
            act.setToolBar(getString(com.publica.ratemyplate.R.string.nav_profile), View.VISIBLE)
        }

        if(activity is MainActivity){
            val act = activity as MainActivity
            act.setToolBar(R.string.nav_profile, View.VISIBLE)

        }

    }


}