package com.publica.ratemyplate.fragment

import android.content.Context
import android.graphics.Typeface
import android.os.Bundle
import android.support.v4.app.FragmentActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import com.publica.ratemyplate.R
import com.publica.ratemyplate.model.ErrorMessage
import com.publica.ratemyplate.model.Register
import com.publica.ratemyplate.network.APIClient
import com.publica.ratemyplate.network.APIInterface
import kotlinx.android.synthetic.main.fragment_registration.view.*
import retrofit2.Call
import retrofit2.Response
import com.google.gson.Gson
import com.publica.ratemyplate.activity.AccountActivity
import com.publica.ratemyplate.data.Constant
import com.publica.ratemyplate.ui.RMPDialog
import com.publica.ratemyplate.util.Util



class RegistrationFragment : BaseFragment() {

    private lateinit var mainView: View
    private lateinit var mainActivity: AccountActivity

    companion object {
        fun show(activity: FragmentActivity) {
            val fragment = RegistrationFragment()
            fragment.clearPush(activity, "", "")

        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(R.layout.fragment_registration, container, false)

        mainActivity = activity as AccountActivity
        mainView = view
        val imm = context!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(mainView.windowToken, 0)

        val font = Typeface.createFromAsset(mainActivity.assets, "fonts/SF-Pro-Text-Semibold.ttf")

        mainView.radio_male.typeface = font
        mainView.radio_female.typeface = font
        mainView.radio_other.typeface = font

        RMPDialog.defaultDialog(mainActivity, mainActivity.getString(R.string.nick_name_info))

        mainView.date_of_birth_et.setOnClickListener {
            Util.onDatePickerClick(view.date_of_birth_et, mainActivity)
        }

        mainView.submit_btn.setOnClickListener {
            getUserRegistrationFields()
        }

        mainView.info_btn.setOnClickListener {
            RMPDialog.defaultDialog(mainActivity, mainActivity.getString(R.string.nick_name_info))
        }

        mainView.terms_and_condition.setOnClickListener {
            TermsAndConditions.show(mainActivity, "")
        }

        mainView.sign_in.setOnClickListener {

            LoginFragment.show(mainActivity)
        }

        mainView.verify_code.setOnClickListener {
            CodeVerificationFragment.show(mainActivity, true)
        }

        return mainView
    }


    private fun getUserRegistrationFields() {

        val firstName = mainView.full_name_et.text.toString()
        val lastName = mainView.last_name_et.text.toString()
        val nickname = mainView.nick_name_et.text.toString()
        val dateOfBirth = mainView.date_of_birth_et.text.toString()
        val city = mainView.city_et.text.toString()
        var gender = Constant.MALE
        val email = mainView.email_et.text.toString()
        val password = mainView.password_et.text.toString()
        val confirmPassword = mainView.confirm_password_et.text.toString()

        if (mainView.radio_sex.checkedRadioButtonId == R.id.radio_female) {
            gender = Constant.FEMALE
        } else if (mainView.radio_sex.checkedRadioButtonId == R.id.radio_other) {
            gender = Constant.OTHER
        }

//        var validationMes = getString(R.string.val_please_enter_ff)
//
//        if(firstName == ""){
//            validationMes = validationMes + getString(R.string.val_first_name)
//        }
//
//        if(lastName == ""){
//            validationMes = validationMes + getString(R.string.val_last_name)
//        }
//
//        if(nickname == ""){
//            validationMes = validationMes + getString(R.string.val_username)
//        }
//
//        if(dateOfBirth == ""){
//            validationMes = validationMes + getString(R.string.val_dateob)
//        }
//
//        if(city == ""){
//            validationMes = validationMes + getString(R.string.val_city)
//        }
//
//        if(email == ""){
//            validationMes = validationMes + getString(R.string.val_email)
//        }
//
//        if(password == ""){
//            validationMes = validationMes + getString(R.string.val_password)
//        }
//
//        if(confirmPassword == ""){
//            validationMes = validationMes + getString(R.string.val_confirm_password)
//        }
//
//        if (validationMes != getString(R.string.val_please_enter_ff)) {
//            RMPDialog.defaultDialog(mainActivity, validationMes)
//            return
//        }

//        if (firstName != "" || lastName != "" || nickname != "" || dateOfBirth != getString(R.string.date_of_birth) || city != "" || email != "" || password != "" || confirmPassword != "") {

        if (password.length < 6) {
            RMPDialog.defaultDialog(mainActivity, getString(R.string.please_enter_6_digits))
            return
        } else {
            if (password != confirmPassword) {
                RMPDialog.defaultDialog(mainActivity, getString(R.string.password_not_match))
                return
            } else {
                if (!mainView.temrs_cb.isChecked) {
                    RMPDialog.defaultDialog(mainActivity, getString(R.string.please_check_terms_and_conditions))
                    return
                } else {

                    if (dateOfBirth == "") {
                        RMPDialog.defaultDialog(mainActivity, "Please enter date of birth")
                        return
                    } else {
                        setupAPI(firstName, lastName, nickname, dateOfBirth, city, gender, email, password)
                    }

                }
            }
        }

//        } else {
//            RMPDialog.defaultDialog(mainActivity, getString(R.string.please_fillup_form))
//        }

    }

    private fun setupAPI(
        firstName: String,
        lastName: String,
        nickname: String,
        dateOfBirth: String,
        city: String,
        gender: String,
        email: String,
        password: String
    ) {

        mainView.submit_btn.isEnabled = false

        val apiInterface = APIClient.getClient(mainActivity).create(APIInterface::class.java)
        val call = apiInterface.postRegister(
            Register(
                firstName,
                lastName,
                nickname,
                Util.dateToEpoch(dateOfBirth, true).toInt(),
                city,
                gender,
                email,
                password
            )
        )
        call.enqueue(object : retrofit2.Callback<Register> {
            override fun onResponse(call: Call<Register>, response: Response<Register>) {

                mainView.submit_btn.isEnabled = true

                if (response.code() != Constant.OK_CODE) {

                    val resourceError = response.errorBody()
                    val errorMessage = Gson().fromJson(resourceError!!.string(), ErrorMessage::class.java)
                    val errors = errorMessage.errors as ErrorMessage.Errors

//                    var emailError = ""
//                    var nicknameError = ""
//
//                    if (errors.email != null) {
//                        emailError = errors.email!!.first()
//                    } else if (errors.nickname != null) {
//                        nicknameError = errors.nickname!!.first()
//                    }
//
//                    val errorMessageForDisplay = errorMessage.message + "\n" + emailError + "\n" + nicknameError


                    if (errorMessage.message == "The given data was invalid.") {

                        var errorMes = ""

                        if (errorMessage.errors!!.email != null) {
                            errorMes = errorMes + errorMessage.errors!!.email!!.first() + "\n"
                        }

                        if (errorMessage.errors!!.nickname != null) {
                            errorMes = errorMes + errorMessage.errors!!.nickname!!.first() + "\n"
                        }

                        if (errorMessage.errors!!.first_name != null) {
                            errorMes = errorMes + errorMessage.errors!!.first_name!!.first() + "\n"
                        }

                        if (errorMessage.errors!!.last_name != null) {
                            errorMes = errorMes + errorMessage.errors!!.last_name!!.first() + "\n"
                        }

                        if (errorMessage.errors!!.age != null) {
                            errorMes = errorMes + errorMessage.errors!!.age!!.first() + "\n"
                        }

                        if (errorMessage.errors!!.gender != null) {
                            errorMes = errorMes + errorMessage.errors!!.gender!!.first() + "\n"
                        }

                        if (errorMessage.errors!!.city != null) {
                            errorMes = errorMes + errorMessage.errors!!.city!!.first() + "\n"
                        }

                        if (errorMessage.errors!!.password != null) {
                            errorMes = errorMes + errorMessage.errors!!.password!!.first() + "\n"
                        }

                        RMPDialog.defaultDialog(mainActivity, errorMes)

                    }

                } else {
                    val resource = response.body()
                    val registerResponse = resource?.message

                    RMPDialog.defaultDialog(mainActivity, mainActivity.getString(R.string.account_creation_successfull))
                    CodeVerificationFragment.show(mainActivity, true)
                }

            }

            override fun onFailure(call: Call<Register>, t: Throwable) {
                if (isAdded) {
                    mainView.submit_btn.isEnabled = true
                    RMPDialog.defaultDialog(mainActivity, mainActivity.getString(R.string.error_mes))
                }
            }
        })
    }

}