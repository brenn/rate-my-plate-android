package com.publica.ratemyplate.fragment

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.view.View
import com.publica.ratemyplate.R
import com.publica.ratemyplate.activity.MainActivity
import kotlinx.android.synthetic.main.activity_account.*


open class BaseFragment : Fragment() {

    private var mToolbarTitle: String? = null
    var toolbarSubtitle: String? = null
        private set

    open var toolbarTitle: String?
        get() = mToolbarTitle
        set(title) {
            mToolbarTitle = title

        }

    fun clearPush(activity: FragmentActivity, title: String, subtitle: String) {
        push(activity, title, subtitle, true)
    }

    @JvmOverloads
    fun push(activity: FragmentActivity?, title: String, subtitle: String, clearStack: Boolean = false) {

        val act = activity as FragmentActivity
        if(!clearStack){
            act.back.visibility = View.VISIBLE
        }else{
            act.back.visibility = View.GONE
        }


        val transaction = activity.supportFragmentManager.beginTransaction()

        if(this is AboutFragment || this is PrivatePolicyFragment || this is TermsAndConditions || this is ResetPasswordFragment || this is ModerationFragment){
            transaction.add(R.id.content_frame, this, CURRENT_FRAGMENT_TAG)
        }else{
            transaction.replace(R.id.content_frame, this, CURRENT_FRAGMENT_TAG)
        }



        if (!clearStack) {
            transaction.addToBackStack(null)
        }

        mToolbarTitle = title
        toolbarSubtitle = subtitle

        transaction.commit()
        fragmentManager!!.executePendingTransactions()

        if ( activity is MainActivity) {
            activity.refreshToolbarState()
        }
    }

    fun onBackPressed(): Boolean {
        return false
    }

    companion object {

        // Used when getting the current fragment by tag
        var CURRENT_FRAGMENT_TAG = "currentFragmentTag"
    }


}