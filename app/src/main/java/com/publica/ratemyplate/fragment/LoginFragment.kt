package com.publica.ratemyplate.fragment

import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.v4.app.FragmentActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import com.publica.ratemyplate.R
import com.publica.ratemyplate.activity.AccountActivity
import com.publica.ratemyplate.activity.MainActivity
import com.publica.ratemyplate.data.Constant
import com.publica.ratemyplate.data.Preference
import com.publica.ratemyplate.model.Login
import com.publica.ratemyplate.network.APIClient
import com.publica.ratemyplate.network.APIInterface
import com.publica.ratemyplate.ui.RMPDialog
import kotlinx.android.synthetic.main.fragment_login.view.*
import retrofit2.Call
import retrofit2.Response

class LoginFragment : BaseFragment() {

    private lateinit var mainActivity: AccountActivity

    companion object {
        fun show(activity: FragmentActivity) {
            val fragment = LoginFragment()
            fragment.push(activity, "", "")

        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(R.layout.fragment_login, container, false)
        mainActivity = activity as AccountActivity

        val imm = context!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)

        view.terms_and_condition.setOnClickListener {
            TermsAndConditions.show(mainActivity, "")
        }

        view.sign_up.setOnClickListener {
            fragmentManager!!.popBackStack()
        }

        view.forgot_password.setOnClickListener {
            ResetPasswordFragment.show(mainActivity)
        }

        view.sign_in_btn.setOnClickListener {

            val emailNickName = view.email_or_nickname_et.text.toString()
            val password = view.password_et.text.toString()

            var validationMes = ""

            if(emailNickName == ""){
                validationMes = validationMes + getString(R.string.val_email_username)
            }

            if(password == ""){
                validationMes = validationMes + getString(R.string.val_password)
            }

            if (validationMes != "") {
            RMPDialog.defaultDialog(mainActivity, validationMes)
            return@setOnClickListener
        }

            view.sign_in_btn.isEnabled = false
            val apiInterface = APIClient.getClient(mainActivity).create(APIInterface::class.java)
            val call = apiInterface.postLogin(
                Login(
                    emailNickName,
                    password,
                    android.os.Build.MODEL + " " + Build.VERSION.RELEASE
                )
            )
            call.enqueue(object : retrofit2.Callback<Login> {

                override fun onResponse(call: Call<Login>, response: Response<Login>) {

                    view.sign_in_btn.isEnabled = true
                    if (response.code() != Constant.OK_CODE) {

//                        val errorMessage = Gson().fromJson(resourceError!!.string(), ErrorMessage::class.java)

                        RMPDialog.defaultDialog(mainActivity, mainActivity.getString(R.string.login_failed))

                    } else {

                        val resource = response.body()
                        val token = resource?.accessToken
                        val userID = resource?.id
                        Preference.token = token!!
                        Preference.userID = userID!!
                        Preference.rememberMe = view.remember_me_cb.isChecked

                        startMainActivity()

                    }
                }

                override fun onFailure(call: Call<Login>, t: Throwable) {
                    if(isAdded){
                        view.sign_in_btn.isEnabled = true
                        RMPDialog.defaultDialog(mainActivity, getString(R.string.error_mes))
                    }
                }

            })

        }

        return view
    }

    private fun startMainActivity() {
        mainActivity.startActivity(Intent(mainActivity, MainActivity::class.java))
        mainActivity.finish()
    }

}