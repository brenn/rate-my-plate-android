package com.publica.ratemyplate.fragment

import android.content.Context
import android.os.Bundle
import android.support.v4.app.FragmentActivity
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import com.publica.ratemyplate.R
import com.publica.ratemyplate.activity.AccountActivity
import com.publica.ratemyplate.activity.MainActivity
import com.publica.ratemyplate.data.Constant
import com.publica.ratemyplate.data.Preference
import com.publica.ratemyplate.model.Password
import com.publica.ratemyplate.network.APIClient
import com.publica.ratemyplate.network.APIInterface
import com.publica.ratemyplate.ui.RMPDialog
import com.publica.ratemyplate.ui.RMPDialog.defaultDialog
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_reset_password.view.*
import retrofit2.Call
import retrofit2.Response

class ResetPasswordFragment : BaseFragment() {
    private lateinit var accountActivity: AppCompatActivity
    companion object {
        fun show(activity: FragmentActivity) {
            val fragment = ResetPasswordFragment()
            fragment.push(activity, activity.getString(R.string.reset_password), "")

        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(R.layout.fragment_reset_password, container, false)

        accountActivity = activity as AppCompatActivity
        accountActivity.toolbar_title.text = getString(R.string.reset_password)

        val imm = context!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)

        if(Preference.email != ""){
            view.reset_password_et.setText(Preference.email)
            view.reset_password_et.isEnabled = false
            view.please_enter_email_tv.visibility = View.GONE
        }

        view.reset_btn.setOnClickListener {

            if(view.reset_password_et.text.toString() == ""){
                defaultDialog(accountActivity, getString(R.string.please_enter_email))
            }

            view.reset_btn.isEnabled = false
            val apiInterface = APIClient.getClient(accountActivity).create(APIInterface::class.java)
            val call = apiInterface.postResetPassword(Password(view.reset_password_et.text.toString()))
            call.enqueue(object : retrofit2.Callback<Password> {

                override fun onResponse(call: Call<Password>, response: Response<Password>) {

                    view.reset_btn.isEnabled = true
                    if (response.code() != Constant.OK_CODE) {

                        val resourceError = response.errorBody()

//                        defaultDialog(accountActivity, "Please try again tomorrow")


                    } else {

                        defaultDialog(accountActivity, accountActivity.getString(R.string.verification_email_has_been_sent))
                        view.reset_password_et.setText("")
                        PasswordCodeVerificationFragment.show(accountActivity, true)

                    }
                }

                override fun onFailure(call: Call<Password>, t: Throwable) {
                    if(isAdded){
                        view.reset_btn.isEnabled = true
                        RMPDialog.defaultDialog(accountActivity, getString(R.string.error_mes))
                    }
                }

            })

        }

        view.verify_code_btn.setOnClickListener {
            PasswordCodeVerificationFragment.show(accountActivity, true)
        }

        return  view
    }

    override fun onDestroy() {
        super.onDestroy()
        Preference.email = ""
    }

    override fun onDestroyView() {
        super.onDestroyView()

//        if(activity is AccountActivity){
//            val act = activity as AccountActivity
//            act.setToolBar(getString(com.publica.ratemyplate.R.string.nav_profile), View.VISIBLE)
//        }

        if(activity is MainActivity){
            val act = activity as MainActivity
            act.setToolBar(com.publica.ratemyplate.R.string.nav_profile, View.VISIBLE)

        }

    }

}