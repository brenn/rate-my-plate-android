package com.publica.ratemyplate.fragment

import android.content.Context
import android.os.Bundle
import android.support.v4.app.FragmentActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import com.publica.ratemyplate.R

import com.publica.ratemyplate.activity.MainActivity
import com.publica.ratemyplate.ui.RMPDialog
import com.publica.ratemyplate.util.Util
import kotlinx.android.synthetic.main.fragment_moderation.view.*


class ModerationFragment : BaseFragment() {

    private lateinit var mainActivity: MainActivity

    companion object {
        fun show(activity: FragmentActivity) {
            val fragment = ModerationFragment()
            fragment.push(activity, activity.getString(R.string.moderation_request), "")

        }
    }

    override fun onDestroy() {
        super.onDestroy()
        mainActivity.setToolBar(R.string.nav_profile, View.VISIBLE)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_moderation, container, false)
        mainActivity = activity as MainActivity

        val imm = context!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)

        if(RMPManager.profile == null)
            return view

        val profile = RMPManager.profile!!
        val contactDetails = profile.firstName + " " + profile.lastName + "\n" + profile.email

        view.username_tv.text = profile.nickname
        view.contact_details_tv.text = contactDetails

        view.submit_btn.setOnClickListener {
            val moderationPN = view.moderation_pn_et.text.toString()
            val moderationSpecificReview = view.moderation_specific_review_et.text.toString()
            val moderationLocation = view.moderation_location_et.text.toString()
            val moderationReason = view.moderation_reason_et.text.toString()

            var validationMes = getString(R.string.val_please_enter_ff)

            if(moderationPN == ""){
                validationMes = validationMes + getString(R.string.val_mod_pn)
            }

            if(moderationSpecificReview == ""){
                validationMes = validationMes + getString(R.string.val_mod_specific_review)
            }

            if(moderationLocation == ""){
                validationMes = validationMes + getString(R.string.val_mod_location)
            }

            if(moderationReason == ""){
                validationMes = validationMes + getString(R.string.val_mod_reason)
            }

            if (validationMes != getString(R.string.val_please_enter_ff)) {
                RMPDialog.defaultDialog(mainActivity, validationMes)
                return@setOnClickListener
            }

            val subject = String.format(getString(R.string.email_subject), profile.nickname)
            val body = String.format(getString(R.string.email_body), Util.getDateNow(), Util.getTmeNow(), profile.nickname, contactDetails, moderationPN, moderationSpecificReview, moderationLocation, moderationReason)
            Util.sendEmail(mainActivity, mainActivity.getString(R.string.rate_my_plate_mail), body, subject)



        }

        RMPDialog.requestModerationDialog(mainActivity, mainActivity.getString(R.string.moderation_message))

        return view

    }

}