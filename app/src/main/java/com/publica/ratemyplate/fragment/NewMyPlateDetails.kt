package com.publica.ratemyplate.fragment

import android.app.ProgressDialog
import android.content.Context
import android.os.Bundle
import android.support.v4.app.FragmentActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import com.publica.ratemyplate.R
import com.publica.ratemyplate.activity.MainActivity
import com.publica.ratemyplate.adapter.CommentAdapter
import com.publica.ratemyplate.adapter.ReviewAdapter
import com.publica.ratemyplate.data.Constant
import com.publica.ratemyplate.data.Preference
import com.publica.ratemyplate.model.PlateNumber
import com.publica.ratemyplate.network.APIClient
import com.publica.ratemyplate.network.APIInterface
import com.publica.ratemyplate.ui.RMPDialog
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_new_plate_details.view.*

import retrofit2.Call
import retrofit2.Response

class NewMyPlateDetails : BaseFragment() {

    private lateinit var mainActivity: MainActivity
    private lateinit var mainView: View
    private var upVote = 0
    private var downVote = 0
    private var neutralVote = 0

    private lateinit var listRatings: ArrayList<PlateNumber.Rating>

    private lateinit var reviewAdapter: ReviewAdapter
//    private lateinit var progressDialog: ProgressDialog

    companion object {
        fun show(activity: FragmentActivity, title: String) {
            val fragment = NewMyPlateDetails()
            activity.filter_plates.visibility = View.GONE
            fragment.push(activity, activity.getString(R.string.plate_display), "")

        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(R.layout.fragment_new_plate_details, container, false)

        mainActivity = activity as MainActivity
        mainActivity.toolbar_title.text = getString(R.string.plate_display)
        mainView = view

        mainView.plate_number_tv.text = RMPManager.searchPlateNumber

        val imm = context!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(mainView.windowToken, 0)

        getPlateDetails(RMPManager.searchPlateNumber!!)





        view.rate_btn.setOnClickListener {
            RMPManager.searchPlateNumber = mainView.plate_number_tv.text.toString()
            RatePlateFragment.show(mainActivity)
        }

        view.watch_btn.setOnClickListener {
            RMPManager.searchPlateNumber = mainView.plate_number_tv.text.toString()
            AddPlateFragment.show(mainActivity)
        }

        return view

    }

    private fun getPlateDetails(plateNumber: String){

        upVote = 0
        downVote = 0
        neutralVote = 0

        val apiInterface = APIClient.getClient(mainActivity).create(APIInterface::class.java)
        val call = apiInterface.getSearchPlate(plateNumber, "ratings", "comments", "comments.user", "comments.moderate_comment")
        call.enqueue(object : retrofit2.Callback<PlateNumber> {

            override fun onResponse(call: Call<PlateNumber>, response: Response<PlateNumber>) {
//                progressDialog.dismiss()
                val myPlateResponse = response.body()
                val message = myPlateResponse?.message

                if (message != null) {

                    if (message == mainActivity.getString(R.string.unauthenticated)) {
                        mainActivity.startLoginActivity()
                    } else {
                        //Proceed
                    }

                } else {
                    val data = myPlateResponse?.data

                    RMPManager.plateNumber = data

                    val plateItem = data!![0]

                    for (rating in plateItem.ratings!!) {

                        if (rating.rating == Constant.LIKE) {
                            upVote++
                        } else if(rating.rating == Constant.DISLIKE) {
                            downVote++
                        }else if(rating.rating == Constant.NEUTRAL){
                            neutralVote++
                        }

                    }

                    mainView.like_tv.text = upVote.toString()
                    mainView.dislike_tv.text = downVote.toString()
                    mainView.neutral_tv.text = neutralVote.toString()

                    mainView.like_btn.visibility = View.VISIBLE
                    mainView.dislike_btn.visibility = View.VISIBLE
                    mainView.neutral_btn.visibility = View.VISIBLE


                    if(data[0].ratings!!.size == 0){

                        mainView.review_recycler_view.adapter = null
                        mainView.like_tv.text = "0"
                        mainView.dislike_tv.text = "0"
                        mainView.neutral_tv.text = "0"
                        mainView.reviews_count.text = "0 Review"
                        return
                    }

                    data[0].ratings!!.reverse()

                    val layoutManager = LinearLayoutManager(activity)
                    mainView.review_recycler_view.layoutManager = layoutManager
                    reviewAdapter = ReviewAdapter(mainActivity, data, object: ReviewAdapter.EventListener{
                        override fun onEvent() {

                           getPlateDetails(RMPManager.searchPlateNumber!!)
                        }
                    })

                    mainView.review_recycler_view.adapter = reviewAdapter

                    listRatings = plateItem.ratings!!
                    listRatings.reverse()

                    val ratingSize = plateItem.ratings!!.size
                    var reviewCtr = mainActivity.getString(R.string.reviews)




                    val hiddenRatingSize = ratingSize - 2

                    if(ratingSize <= 2){

                        mainView.next_feature_layout.visibility = View.GONE

                    }else if(data[0].ratings!![0].userId == Preference.userID && data[0].ratings!!.size <= 3){
                        mainView.next_feature_layout.visibility = View.GONE
                    }
                    else{
                        mainView.next_feature_layout.visibility = View.VISIBLE
//                        mainView.unlock_tv.text =  "$mes_1 $hiddenRatingSize $mes_2"
                    }

                    if (ratingSize == 1) {
                        reviewCtr = "$ratingSize $reviewCtr"
                    } else if (ratingSize > 1) {
                        reviewCtr = "" + ratingSize + " " + reviewCtr + "s "
                    }

                    mainView.reviews_count.text = reviewCtr

                }

            }

            override fun onFailure(call: Call<PlateNumber>, t: Throwable) {
                if(isAdded){
                    RMPDialog.defaultDialog(mainActivity, mainActivity.getString(R.string.error_mes))
                }
//                progressDialog.dismiss()

            }

        })
    }



}