package com.publica.ratemyplate.fragment

import android.content.Context
import android.os.Bundle
import android.support.v4.app.FragmentActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import com.publica.ratemyplate.R
import com.publica.ratemyplate.activity.MainActivity
import com.publica.ratemyplate.ui.RMPDialog
import kotlinx.android.synthetic.main.fragment_search_plate.view.*

class SearchPlateFragment : BaseFragment() {

    companion object {
        fun show(activity: FragmentActivity) {
            val fragment = SearchPlateFragment()
            fragment.push(activity, activity.getString(R.string.search_plate), "")
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(R.layout.fragment_search_plate, container, false)
        val act: MainActivity = activity as MainActivity
        act.setToolBar(R.string.search_plate, View.VISIBLE)

        if(RMPManager.searchPlateNumber == null) {
            RMPDialog.defaultDialog(act, getString(R.string.error_mes))
            return null
        }

        view.plate_number.text = RMPManager.searchPlateNumber

        val imm = context!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)

//        if (RMPManager.plateNumber?.size != 0 ) {
//            //to do
//            view.review_plate_mes.visibility = View.GONE
//
//        }

        view.rate_btn.setOnClickListener {
            RatePlateFragment.show(activity!!)
        }

        view.add_btn.setOnClickListener {
            AddPlateFragment.show(activity!!)
        }

        return view
    }

}