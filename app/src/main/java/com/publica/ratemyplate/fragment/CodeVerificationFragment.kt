package com.publica.ratemyplate.fragment

import android.content.Context
import android.os.Bundle
import android.support.v4.app.FragmentActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import com.google.gson.Gson
import com.publica.ratemyplate.R
import com.publica.ratemyplate.activity.AccountActivity
import com.publica.ratemyplate.activity.MainActivity
import com.publica.ratemyplate.data.Constant
import com.publica.ratemyplate.model.ErrorMessage
import com.publica.ratemyplate.model.Verify
import com.publica.ratemyplate.network.APIClient
import com.publica.ratemyplate.network.APIInterface
import com.publica.ratemyplate.ui.RMPDialog
import kotlinx.android.synthetic.main.fragment_code_verification.view.*
import retrofit2.Call
import retrofit2.Response

class CodeVerificationFragment : BaseFragment() {

    private lateinit var mainActivity: AccountActivity

    companion object {
        fun show(activity: FragmentActivity, pushOnly: Boolean) {
            val fragment = CodeVerificationFragment()
            if(pushOnly){
                fragment.push(activity, "", "")
            }else{
                fragment.clearPush(activity, "", "")
            }


        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_code_verification, container, false)

        mainActivity = activity as AccountActivity
        val imm = context!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)

        if(RMPManager.code != null){
            view.verify_code_et.setText(RMPManager.code)
            RMPManager.code = null
            RMPManager.screen = null
        }

        view.verify_btn.setOnClickListener {

            val code = view.verify_code_et.text.toString()

            if(code == ""){
                RMPDialog.defaultDialog(mainActivity, getString(R.string.please_enter_the_code))
                return@setOnClickListener
            }
            view.verify_btn.isEnabled = false
            val apiInterface = APIClient.getClient(mainActivity).create(APIInterface::class.java)
            val call = apiInterface.postVerify(Verify(code))
            call.enqueue(object : retrofit2.Callback<Verify> {

                override fun onResponse(call: Call<Verify>, response: Response<Verify>) {

                    view.verify_btn.isEnabled = true

                    if (response.code() != Constant.OK_CODE) {

                        val resourceError = response.errorBody()
                        val errorMessage = Gson().fromJson(resourceError!!.string(), ErrorMessage::class.java)

                        RMPDialog.defaultDialog(mainActivity, errorMessage.error!!)

                    } else {

                        val resource = response.body()
                        RMPDialog.defaultDialog(mainActivity, resource?.message!!)
                        val fm = mainActivity.supportFragmentManager
                        val count = fm.backStackEntryCount
                        for (i in 0 until count) {
                            mainActivity.supportFragmentManager.popBackStack()
                        }
                        LoginFragment.show(mainActivity)
                        RMPManager.code = null

                    }

                }

                override fun onFailure(call: Call<Verify>, t: Throwable) {
                    if(isAdded){
                        view.verify_btn.isEnabled = true
                        RMPDialog.defaultDialog(mainActivity, getString(R.string.error_mes))
                    }
                }

            })

        }

        return view
    }


}