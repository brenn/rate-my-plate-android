package com.publica.ratemyplate.fragment


import RMPManager.family
import android.content.Context
import android.os.Bundle
import android.support.v4.app.FragmentActivity

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import com.publica.ratemyplate.R
import com.publica.ratemyplate.activity.MainActivity
import com.publica.ratemyplate.model.PlateNumber
import com.publica.ratemyplate.network.APIClient
import com.publica.ratemyplate.network.APIInterface
import com.publica.ratemyplate.ui.RMPDialog
import kotlinx.android.synthetic.main.fragment_my_plate_empty.view.*
import kotlinx.android.synthetic.main.fragment_search_plate.view.*
import retrofit2.Call
import retrofit2.Response

class MyPlateEmptyFragment : BaseFragment() {

    private lateinit var mainActivity: MainActivity

    companion object {

        lateinit var mes: String

        fun show(activity: FragmentActivity, message: String) {
            val fragment = MyPlateEmptyFragment()
            mes = message
            fragment.clearPush(activity, activity.getString(R.string.nav_my_plates), "")

        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(R.layout.fragment_my_plate_empty, container, false)
        mainActivity = activity as MainActivity
        mainActivity.setToolBar(R.string.nav_my_plates, View.VISIBLE)

        val imm = context!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)

        view.search_mes_tv.text = mes

        view.search_plate_btn.setOnClickListener {

            val plateNumber = view.plate_number_et.text.toString()

            if (plateNumber == "") {
                RMPDialog.defaultDialog(mainActivity, getString(R.string.please_enter_plate_number))
                return@setOnClickListener
            }

//            for(family in RMPManager.family!!){
//                if(family.plateNumber!!.plateNumber == plateNumber){
//
//                    RMPManager.selectedPlateNumber = family
//
//                    PlateDetailsFragment.show(mainActivity, getString(R.string.nav_my_plates))
//
//                    return@setOnClickListener
//                }
//            }
//
//            for(other in RMPManager.others!!){
//                if(other.plateNumber!!.plateNumber == plateNumber){
//
//                    RMPManager.selectedPlateNumber = other
//
//                    PlateDetailsFragment.show(mainActivity, getString(R.string.nav_my_plates))
//
//                    return@setOnClickListener
//                }
//            }

            view.search_plate_btn.isEnabled = false
            val apiInterface = APIClient.getClient(activity!!).create(APIInterface::class.java)
            val call = apiInterface.getSearchPlate(view.plate_number_et.text.toString(), "ratings", "comments", "comments.user", "comments.moderate_comment")
            call.enqueue(object : retrofit2.Callback<PlateNumber> {

                override fun onResponse(call: Call<PlateNumber>, response: Response<PlateNumber>) {
                    view.search_plate_btn.isEnabled = true
                    val myPlateResponse = response.body()
                    val message = myPlateResponse?.message

                    if (message != null) {

                        if (message == mainActivity.getString(R.string.unauthenticated)) {
                            mainActivity.startLoginActivity()
                        } else {
                            //Proceed
                        }

                    } else {
                        val data = myPlateResponse?.data

                        RMPManager.plateNumber = data
                        RMPManager.searchPlateNumber = view.plate_number_et.text.toString()


                        if (RMPManager.plateNumber?.size != 0 ) {
                            //to do
//                            view.review_plate_mes.visibility = View.GONE
//                            PlateDetails2Fragment.show(mainActivity, getString(R.string.nav_my_plates))
                            NewMyPlateDetails.show(mainActivity, mainActivity.getString(R.string.nav_my_plates))
                        }else{
                            SearchPlateFragment.show(mainActivity)
                        }

                    }

                }

                override fun onFailure(call: Call<PlateNumber>, t: Throwable) {
                    if(isAdded){
                        view.search_plate_btn.isEnabled = true
                        RMPDialog.defaultDialog(mainActivity, getString(R.string.error_mes))
                    }
                }

            })

        }

        return view
    }

}