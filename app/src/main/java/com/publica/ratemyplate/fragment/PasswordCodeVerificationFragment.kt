package com.publica.ratemyplate.fragment

import android.content.Context
import android.os.Bundle
import android.support.v4.app.FragmentActivity
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import com.google.gson.Gson
import com.publica.ratemyplate.R
import com.publica.ratemyplate.activity.AccountActivity
import com.publica.ratemyplate.activity.MainActivity
import com.publica.ratemyplate.data.Constant
import com.publica.ratemyplate.data.Preference
import com.publica.ratemyplate.model.ErrorMessage
import com.publica.ratemyplate.model.Verify
import com.publica.ratemyplate.network.APIClient
import com.publica.ratemyplate.network.APIInterface
import com.publica.ratemyplate.ui.RMPDialog
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_password_code_verification.view.*
import retrofit2.Call
import retrofit2.Response

class PasswordCodeVerificationFragment : BaseFragment() {

    private lateinit var mainActivity: AppCompatActivity

    companion object {
        fun show(activity: FragmentActivity, isPushOnly: Boolean) {
            val fragment = PasswordCodeVerificationFragment()
            if(isPushOnly){
                fragment.push(activity, activity.getString(R.string.reset_password), "")
            }else{
                fragment.clearPush(activity, activity.getString(R.string.reset_password), "")
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_password_code_verification, container, false)

        mainActivity = activity as AppCompatActivity
        mainActivity.toolbar_title.text = getString(R.string.reset_password)

        val imm = context!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)

        if(RMPManager.code != null){
            view.verify_code_et.setText(RMPManager.code)
            RMPManager.code = null
            RMPManager.screen = null
        }

        RMPManager.code = null

        view.verify_btn.setOnClickListener {

            val code = view.verify_code_et.text.toString()
            val password = view.password.text.toString()
            val confirmPassword = view.confirm_password.text.toString()

            if(code == ""){
                RMPDialog.defaultDialog(mainActivity, "The verification field is required.")
                return@setOnClickListener
            }

            if(  password == "" && confirmPassword == ""){
                RMPDialog.defaultDialog(mainActivity, "The password fields are required.")
                return@setOnClickListener
            }

            if(password.length < 6){
                RMPDialog.defaultDialog(mainActivity, getString(R.string.please_enter_6_digits))
                return@setOnClickListener
            }

            if(password != confirmPassword){
                RMPDialog.defaultDialog(mainActivity, getString(R.string.password_not_match))
                return@setOnClickListener
            }
            view.verify_btn.isEnabled = false
            val apiInterface = APIClient.getClient(mainActivity).create(APIInterface::class.java)
            val call = apiInterface.postUpdatePassword(Verify(code, password))
            call.enqueue(object : retrofit2.Callback<Verify> {

                override fun onResponse(call: Call<Verify>, response: Response<Verify>) {
                    view.verify_btn.isEnabled = true
                    if (response.code() != Constant.OK_CODE) {

                        val resourceError = response.errorBody()
                        val errorMessage = Gson().fromJson(resourceError!!.string(), ErrorMessage::class.java)

                        RMPDialog.defaultDialog(mainActivity, errorMessage.message!!)

                    } else {

                        val resource = response.body()
                        RMPDialog.defaultDialog(mainActivity, resource?.message!!)
                        if(Preference.token != ""){
                            view.verify_code_et.setText("")
                            view.password.setText("")
                            view.confirm_password.setText("")
                        }else{
                            if(mainActivity is AccountActivity){

                                val fm = mainActivity.supportFragmentManager
                                val count = fm.backStackEntryCount
                                for (i in 0 until count) {
                                    mainActivity.supportFragmentManager.popBackStack()
                                }

                                LoginFragment.show(mainActivity)


                            }else {
                                ProfileFragment.show(mainActivity)
//                                fragmentManager!!.popBackStack()
                            }

                        }

                    }

                }

                override fun onFailure(call: Call<Verify>, t: Throwable) {
                    if(isAdded){
                        view.verify_btn.isEnabled = true
                        RMPDialog.defaultDialog(mainActivity, getString(R.string.error_mes))
                    }
                }

            })

        }

        return view
    }



}