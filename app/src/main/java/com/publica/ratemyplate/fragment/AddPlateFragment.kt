package com.publica.ratemyplate.fragment

import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Context
import android.content.DialogInterface
import android.os.Bundle

import android.support.v4.app.FragmentActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import com.google.gson.Gson
import com.publica.ratemyplate.R
import com.publica.ratemyplate.activity.MainActivity
import com.publica.ratemyplate.data.Constant
import com.publica.ratemyplate.data.Preference
import com.publica.ratemyplate.model.ErrorMessage
import com.publica.ratemyplate.model.Rating
import com.publica.ratemyplate.network.APIClient
import com.publica.ratemyplate.network.APIInterface
import com.publica.ratemyplate.ui.RMPDialog
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.dialog_message.view.*
import kotlinx.android.synthetic.main.fragment_add_plate.view.*
import retrofit2.Call
import retrofit2.Response

class AddPlateFragment : BaseFragment() {

    private var isFamily: Int = Constant.FAMILY
    private lateinit var progressDialog: ProgressDialog
    private lateinit var mainActivity: MainActivity

    companion object {
        fun show(activity: FragmentActivity) {
            val fragment = AddPlateFragment()
            fragment.push(activity, activity.getString(R.string.add_plate), "")

        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(R.layout.fragment_add_plate, container, false)
        mainActivity = activity as MainActivity
        mainActivity.navigation.itemBackgroundResource = R.drawable.footermainmenulight

        val imm = context!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)

        if(RMPManager.searchPlateNumber == null) {
            RMPDialog.defaultDialog(mainActivity, getString(R.string.error_mes))
            return null
        }

        view.plate_number_tv.text = RMPManager.searchPlateNumber


        view.family_toggle.setOnClickListener {

            if(isFamily == Constant.OTHERS){
                isFamily = Constant.FAMILY
                view.family_toggle.setImageResource(R.drawable.selected)
                view.other_toggle.setImageResource(R.drawable.notselected)
            }else{
                isFamily = Constant.OTHERS
                view.other_toggle.setImageResource(R.drawable.selected)
                view.family_toggle.setImageResource(R.drawable.notselected)
            }

        }

        view.other_toggle.setOnClickListener {
            if(isFamily == Constant.OTHERS){
                isFamily = Constant.FAMILY
                view.family_toggle.setImageResource(R.drawable.selected)
                view.other_toggle.setImageResource(R.drawable.notselected)
            }else{
                isFamily = Constant.OTHERS
                view.other_toggle.setImageResource(R.drawable.selected)
                view.family_toggle.setImageResource(R.drawable.notselected)
            }
        }

        view.submit_btn.setOnClickListener {

            val plateName = view.plate_name_et.text.toString()

            progressDialog = ProgressDialog.show(mainActivity, getString(R.string.please_wait), getString(R.string.submitting_plate_number), true)
            view.submit_btn.isEnabled = false
            val apiInterface = APIClient.getClient(mainActivity).create(APIInterface::class.java)
            val call = apiInterface.postAddPlateNumber(
                Rating(RMPManager.searchPlateNumber!!, plateName, isFamily)
            )
            call.enqueue(object : retrofit2.Callback<Rating.Response> {

                override fun onResponse(call: Call<Rating.Response>, response: Response<Rating.Response>) {

                    progressDialog.dismiss()
                    view.submit_btn.isEnabled = true
                    if (response.code() != Constant.OK_CODE) {

                        val resourceError = response.errorBody()
                        val errorMessage = Gson().fromJson(resourceError!!.string(), ErrorMessage::class.java)

                        if (errorMessage.message == mainActivity.getString(R.string.unauthenticated)) {
                            mainActivity.startLoginActivity()
                        }else{
                            RMPDialog.defaultDialog(mainActivity, errorMessage!!.message!!)
                        }

                    } else {
                        defaultAddPlateDialog(mainActivity, mainActivity.getString(R.string.thank_you_for_adding_this_plate))


                    }
                }

                override fun onFailure(call: Call<Rating.Response>, t: Throwable) {
                    if(isAdded){
                        progressDialog.dismiss()
                        view.submit_btn.isEnabled = true
                        RMPDialog.defaultDialog(mainActivity, getString(R.string.error_mes))
                    }

                }

            })

        }

        return view
    }

    fun defaultAddPlateDialog(activity: FragmentActivity, message: String) {

        val builder = AlertDialog.Builder(activity)
        builder.setPositiveButton(activity.getString(R.string.ok), DialogInterface.OnClickListener { _, _ ->
            if(Preference.firstAdd){
                defaultAddPlateSecondDialog(mainActivity, mainActivity.getString(R.string.added_plate_second_mes))
                Preference.firstAdd = false
            }else{
                mainActivity.navigation.selectedItemId = R.id.navigation_my_plate
            }

        })

        val inflater = activity.layoutInflater
        val view = inflater.inflate(R.layout.dialog_message, null)
        view.dialog_mess.text = message
        val dialog = builder.create()
        dialog.setView(view)
        dialog.show()

    }

    fun defaultAddPlateSecondDialog(activity: FragmentActivity, message: String) {

        val builder = AlertDialog.Builder(activity)
        builder.setPositiveButton(activity.getString(R.string.ok), DialogInterface.OnClickListener { _, _ ->
            mainActivity.navigation.selectedItemId = R.id.navigation_my_plate
        })

        val inflater = activity.layoutInflater
        val view = inflater.inflate(R.layout.dialog_message, null)
        view.dialog_mess.text = message
        val dialog = builder.create()
        dialog.setView(view)
        dialog.show()

    }

    override fun onDestroy() {
        super.onDestroy()
        mainActivity.navigation.itemBackgroundResource = R.drawable.footermainmenu
    }

}