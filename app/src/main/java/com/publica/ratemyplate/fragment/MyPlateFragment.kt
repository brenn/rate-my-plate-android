package com.publica.ratemyplate.fragment

import android.content.Context
import android.os.Bundle
import android.support.v4.app.FragmentActivity
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import com.google.gson.Gson
import com.publica.ratemyplate.R
import com.publica.ratemyplate.activity.MainActivity
import com.publica.ratemyplate.adapter.PlateAdapter
import com.publica.ratemyplate.data.Constant
import com.publica.ratemyplate.model.ErrorMessage
import com.publica.ratemyplate.model.MyPlate
import com.publica.ratemyplate.network.APIClient
import com.publica.ratemyplate.network.APIInterface
import com.publica.ratemyplate.ui.RMPDialog
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_my_plate.view.*
import retrofit2.Call
import retrofit2.Response

class MyPlateFragment : BaseFragment(), MainActivity.UpdateDataInterface {

    private lateinit var tempSelectedFilter: ImageView
    private lateinit var selectedFilter: ImageView
    private lateinit var mainView: View
    private lateinit var mainActivity: MainActivity
    private var isFilterLayoutVisible: Boolean? = false
    private var isFamilyBtnSelected = true
    private var filter = Constant.DEFAULT

    private lateinit var familyAdapter:PlateAdapter
    private lateinit var othersAdapter:PlateAdapter

    companion object {
        fun show(activity: FragmentActivity) {
            val fragment = MyPlateFragment()
            fragment.clearPush(activity, activity.getString(R.string.nav_my_plates), "")
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(R.layout.fragment_my_plate, container, false)
        mainActivity = activity as MainActivity
        mainView = view

        val imm = context!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(mainView.windowToken, 0)
        //make this visible if ever
        mainActivity.filter_plates.visibility = View.INVISIBLE
        setupFilter()

        val familyLayoutManager = LinearLayoutManager(activity)
        view.family_recycler_view.layoutManager = familyLayoutManager
        familyAdapter = PlateAdapter(activity as FragmentActivity, RMPManager.family!!)
        view.family_recycler_view.adapter = familyAdapter

        val othersLayoutManager = LinearLayoutManager(activity)
        view.others_recycler_view.layoutManager = othersLayoutManager
        othersAdapter = PlateAdapter(activity as FragmentActivity, RMPManager.others!!)
        view.others_recycler_view.adapter = othersAdapter

        view.family_tab.setOnClickListener {
            isFamilyBtnSelected = true
            showRecyclerView()
        }

        view.other_tab.setOnClickListener {
            isFamilyBtnSelected = false
            showRecyclerView()
        }


        return view
    }



    private fun setupFilter() {

        tempSelectedFilter = mainView.default_cb
        selectedFilter = mainView.default_cb

        mainView.default_layout.setOnClickListener {
            assignFilter(mainView.default_cb)
        }

        mainView.commented_on_layout.setOnClickListener {
            assignFilter(mainView.commented_on_cb)
        }

        mainView.like_layout.setOnClickListener {
            assignFilter(mainView.like_cb)

        }

        mainView.dislike_layout.setOnClickListener {
            assignFilter(mainView.dislike_cb)
        }

        mainView.apply_filter.setOnClickListener {
            selectedFilter = tempSelectedFilter
            setFilterLayoutVisibility(false)

            when(selectedFilter){
                mainView.like_cb ->   getFilteredRatingMyPlates(Constant.LIKE)
                mainView.dislike_cb -> getFilteredRatingMyPlates(Constant.DISLIKE)
                mainView.commented_on_cb ->  getFilteredRatingMyPlates(Constant.NEUTRAL)
                mainView.default_cb -> getMyPlates()
            }
        }

    }

    private fun assignFilter(selectedItem: ImageView) {
        selectedFilter.visibility = View.GONE
        tempSelectedFilter.visibility = View.GONE
        selectedItem.visibility = View.VISIBLE
        tempSelectedFilter = selectedItem
    }

    private fun setFilterLayoutVisibility(isVisible: Boolean) {

        mainActivity.setToolbarFilter(isVisible)
        isFilterLayoutVisible = isVisible

        if (isVisible) {
            mainView.filter_layout!!.visibility = View.VISIBLE
            mainView.dim_layout!!.visibility = View.VISIBLE
        } else {
            mainView.filter_layout!!.visibility = View.GONE
            mainView.dim_layout!!.visibility = View.GONE
        }
    }

    override fun onUpdate(filter: String) {

    }

    override fun onShowFilterLayout() {
        if (isFilterLayoutVisible!!) {
            setFilterLayoutVisibility(false)
            tempSelectedFilter.visibility = View.GONE
            selectedFilter.visibility = View.VISIBLE
        } else {
            setFilterLayoutVisibility(true)
        }

    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        (context as MainActivity).mListener = this
    }

    override fun onDestroy() {
        super.onDestroy()
        mainActivity.filter_plates.visibility = View.GONE
        mainActivity.setToolbarFilter(false)
    }

    override fun onResume() {
        super.onResume()

        showRecyclerView()
        if(filter == Constant.DEFAULT){
            getMyPlates()
        }else{
            getFilteredRatingMyPlates(filter)
        }

    }

    private fun showRecyclerView() {

        if (isFamilyBtnSelected) {

            mainView.family_recycler_view.visibility = View.VISIBLE
            mainView.others_recycler_view.visibility = View.GONE
            mainView.family_tab.setTextColor(ContextCompat.getColor(mainActivity, android.R.color.white))
            mainView.other_tab.setTextColor(ContextCompat.getColor(mainActivity, R.color.rmp_blue))
            mainView.tabs_layout.setBackgroundResource(R.drawable.tabsplatesleft)

        } else {

            mainView.family_recycler_view.visibility = View.GONE
            mainView.others_recycler_view.visibility = View.VISIBLE
            mainView.family_tab.setTextColor(ContextCompat.getColor(mainActivity, R.color.rmp_blue))
            mainView.other_tab.setTextColor(ContextCompat.getColor(mainActivity, android.R.color.white))
            mainView.tabs_layout.setBackgroundResource(R.drawable.tabsplatesright)

        }

    }

    private fun getMyPlates() {
        filter = Constant.DEFAULT
        val apiInterface = APIClient.getClient(mainActivity).create(APIInterface::class.java)
        val call = apiInterface.getMyPlates(
            "plate_number.comments.comment_attachments",
            "plate_number.ratings",
            "plate_number.alerts",
            "plate_number.comments.user",
            "plate_number.comments.moderate_comment"
        )
        call.enqueue(object : retrofit2.Callback<MyPlate> {

            override fun onResponse(call: Call<MyPlate>, response: Response<MyPlate>) {

                if (response.code() != Constant.OK_CODE) {

                    val resourceError = response.errorBody()
                    val errorMessage = Gson().fromJson(resourceError!!.string(), ErrorMessage::class.java)

                    if (errorMessage.message == mainActivity.getString(R.string.unauthenticated)) {
                        mainActivity.startLoginActivity()
                    }

                } else {

                    val resource = response.body()
                    val closeOne = resource?.closeOne
                    val others = resource?.others

                    RMPManager.family = closeOne
                    RMPManager.others = others

                    familyAdapter.updateList(closeOne!!)
                    othersAdapter.updateList(others!!)
                }
            }

            override fun onFailure(call: Call<MyPlate>, t: Throwable) {
                if(isAdded){
                    RMPDialog.defaultDialog(mainActivity, getString(R.string.error_mes))
                }
            }

        })

    }

    private fun getFilteredRatingMyPlates(rating: Int) {
        filter = rating
        val apiInterface = APIClient.getClient(mainActivity).create(APIInterface::class.java)
        val call = apiInterface.getFilteredByRatingMyPlates(
            rating,
            "plate_number.comments.comment_attachments",
            "plate_number.ratings",
            "plate_number.alerts",
            "plate_number.comments.user",
            "plate_number.comments.moderate_comment"
        )
        call.enqueue(object : retrofit2.Callback<MyPlate> {

            override fun onResponse(call: Call<MyPlate>, response: Response<MyPlate>) {

                if (response.code() != Constant.OK_CODE) {

                    val resourceError = response.errorBody()
                    val errorMessage = Gson().fromJson(resourceError!!.string(), ErrorMessage::class.java)

                    if (errorMessage.message == mainActivity.getString(R.string.unauthenticated)) {
                        mainActivity.startLoginActivity()
                    }

                } else {

                    val resource = response.body()
                    val closeOne = resource?.closeOne
                    val others = resource?.others

                    RMPManager.family = closeOne
                    RMPManager.others = others

                    familyAdapter.updateList(closeOne!!)
                    othersAdapter.updateList(others!!)
                }
            }

            override fun onFailure(call: Call<MyPlate>, t: Throwable) {
                if(isAdded){
                    RMPDialog.defaultDialog(mainActivity, getString(R.string.error_mes))
                }

            }

        })

    }

    private fun getFilteredByCommentMyPlates(comment: Int) {

        val apiInterface = APIClient.getClient(mainActivity).create(APIInterface::class.java)
        val call = apiInterface.getFilteredbyCommentMyPlates(
            comment,
            "plate_number.comments.comment_attachments",
            "plate_number.ratings",
            "plate_number.alerts",
            "plate_number.comments.user",
            "plate_number.comments.moderate_comment"
        )
        call.enqueue(object : retrofit2.Callback<MyPlate> {

            override fun onResponse(call: Call<MyPlate>, response: Response<MyPlate>) {

                if (response.code() != Constant.OK_CODE) {

                    val resourceError = response.errorBody()
                    val errorMessage = Gson().fromJson(resourceError!!.string(), ErrorMessage::class.java)

                    if (errorMessage.message == getString(R.string.unauthenticated)) {
                        mainActivity.startLoginActivity()
                    }

                } else {

                    val resource = response.body()
                    val closeOne = resource?.closeOne
                    val others = resource?.others

                    RMPManager.family = closeOne
                    RMPManager.others = others

                    familyAdapter.updateList(closeOne!!)
                    othersAdapter.updateList(others!!)
                }
            }

            override fun onFailure(call: Call<MyPlate>, t: Throwable) {
                if(isAdded){
                    RMPDialog.defaultDialog(mainActivity, getString(R.string.error_mes))
                }

            }

        })

    }

}