package com.publica.ratemyplate.fragment

import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.media.ThumbnailUtils
import android.os.Bundle
import android.support.v4.app.FragmentActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.ImageButton
import com.google.gson.Gson
import com.publica.ratemyplate.R
import com.publica.ratemyplate.activity.MainActivity
import com.publica.ratemyplate.adapter.CommentAdapter
import com.publica.ratemyplate.data.Constant
import com.publica.ratemyplate.model.Alert
import com.publica.ratemyplate.model.Comment
import com.publica.ratemyplate.model.ErrorMessage
import com.publica.ratemyplate.model.Rating
import com.publica.ratemyplate.network.APIClient
import com.publica.ratemyplate.network.APIInterface
import com.publica.ratemyplate.ui.CustomFontTextView
import com.publica.ratemyplate.ui.RMPDialog
import com.publica.ratemyplate.ui.RMPDialog.defaultDialog
import com.publica.ratemyplate.util.Util
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.dialog_add_comment.view.*
import kotlinx.android.synthetic.main.fragment_plate_details.view.*
import okhttp3.MediaType
import retrofit2.Call
import retrofit2.Response
import okhttp3.RequestBody
import pl.aprilapps.easyphotopicker.EasyImage
import java.io.File
import pl.aprilapps.easyphotopicker.DefaultCallback
import java.io.ByteArrayOutputStream
import kotlin.collections.ArrayList


class PlateDetailsFragment : BaseFragment() {

    private lateinit var mainActivity: MainActivity
    private lateinit var mainView: View
    private var upVote = 0
    private var downVote = 0
    private lateinit var img1: ImageButton
    private lateinit var img2: ImageButton
    private lateinit var img3: ImageButton

    private lateinit var listComments: ArrayList<Comment>

    private var img1Path = ""
    private var img2Path = ""
    private var img3Path = ""

    private lateinit var img1Bmp: Bitmap
    private lateinit var img2Bmp: Bitmap
    private lateinit var img3Bmp: Bitmap

    private var imageTotal = 0
    private var imageCtr = 0

    private lateinit var commentAdapter: CommentAdapter
    private lateinit var progressDialog: ProgressDialog

    companion object {
        fun show(activity: FragmentActivity, title: String) {
            val fragment = PlateDetailsFragment()
            activity.filter_plates.visibility = View.GONE
            fragment.push(activity, activity.getString(R.string.plate_display), "")

        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(R.layout.fragment_plate_details, container, false)

        mainActivity = activity as MainActivity
        mainActivity.toolbar_title.text = getString(R.string.plate_display)
        mainView = view

        val imm = context!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(mainView.windowToken, 0)

        val plateItem = RMPManager.selectedPlateNumber!!

        updateAlert(plateItem.id!!)

        for (rating in plateItem.plateNumber!!.ratings!!) {

            if (rating.rating == Constant.LIKE) {
                upVote++
            } else {
                downVote++
            }

        }
        listComments = plateItem.plateNumber!!.comments!!
        listComments.reverse()

        val commentSize = plateItem.plateNumber!!.comments!!.size
        var commentCtr = getString(R.string.comment)

        view.plate_number_tv.text = plateItem.plateNumber!!.plateNumber.toString()
        view.like_tv.text = upVote.toString()
        view.dislike_tv.text = downVote.toString()

        val mes_1 = getString(R.string.unlock_comments_mes_1)
        val mes_2 = getString(R.string.unlock_comments_mes_2)
        val hiddenCommentSize = commentSize - 2

        if(commentSize <= 2){
            view.unlock_tv.visibility = View.GONE
            view.view_all_btn.visibility = View.GONE
            view.not_available_tv.visibility = View.GONE

        }else{
            view.unlock_tv.text =  "$mes_1 $hiddenCommentSize $mes_2"
        }

        if (commentSize == 1) {
            commentCtr = "$commentCtr $commentSize"
        } else if (commentSize > 1) {
            commentCtr = commentCtr + "s " + commentSize
        }

        view.comments_ctr.text = commentCtr

        val layoutManager = LinearLayoutManager(activity)

        view.comment_recycler_view.layoutManager = layoutManager
        commentAdapter = CommentAdapter(mainActivity, plateItem.plateNumber!!.comments!!, view.comments_ctr)
        view.comment_recycler_view.adapter = commentAdapter

//        view.like_btn.setOnClickListener {
//            likeDislikePlateNumber(plateItem.plateNumberId!!, Constant.LIKE)
//        }

//        view.dislike_btn.setOnClickListener {
//            likeDislikePlateNumber(plateItem.plateNumberId!!, Constant.DISLIKE)
//        }

        view.rate_btn.setOnClickListener {
            RMPManager.searchPlateNumber = mainView.plate_number_tv.text.toString()
            RatePlateFragment.show(mainActivity)
        }

        view.watch_btn.setOnClickListener {
            RMPManager.searchPlateNumber = mainView.plate_number_tv.text.toString()
            AddPlateFragment.show(mainActivity)
        }

        view.comment_btn.setOnClickListener {

            addCommentDialog(
                mainActivity,
                getString(R.string.add_comment),
                plateItem.plateNumberId!!
            )

        }

        return view

    }

    private fun likeDislikePlateNumber(plateNumberID: Int, rating: Int) {
        val apiInterface = APIClient.getClient(mainActivity).create(APIInterface::class.java)
        val call = apiInterface.postRatingOnly(Rating(plateNumberID, rating))
        call.enqueue(object : retrofit2.Callback<Rating> {

            override fun onResponse(call: Call<Rating>, response: Response<Rating>) {

                if (response.code() != Constant.OK_CODE) {

                    val resourceError = response.errorBody()
                    val errorMessage = Gson().fromJson(resourceError!!.string(), ErrorMessage::class.java)

                    RMPDialog.defaultDialog(mainActivity, mainActivity.getString(R.string.please_try_again_tomorrow))

                    if (errorMessage.message == mainActivity.getString(R.string.unauthenticated)) {
                        mainActivity.startLoginActivity()
                    }
                } else {

                    if (rating == -1) {
                        downVote++
                        mainView.dislike_tv.text = downVote.toString()

                    } else {
                        upVote++
                        mainView.like_tv.text = upVote.toString()

                    }

                }
            }

            override fun onFailure(call: Call<Rating>, t: Throwable) {
                RMPDialog.defaultDialog(mainActivity, getString(R.string.error_mes))
            }

        })
    }

    private fun addCommentDialog(
        activity: FragmentActivity,
        message: String,
        plateNumberID: Int
    ) {

        val builder = AlertDialog.Builder(activity)

        val inflater = activity.layoutInflater
        val view = inflater.inflate(R.layout.dialog_add_comment, null)
        view.dialog_mess.text = message

        img1 = view.img_1
        img2 = view.img_2
        img3 = view.img_3

        img1.setOnClickListener {
            EasyImage.openGallery(this, 1)
        }
        img2.setOnClickListener {
            EasyImage.openGallery(this, 2)
        }
        img3.setOnClickListener {
            EasyImage.openGallery(this, 3)
        }

        builder.setPositiveButton(
            activity.getString(R.string.post_comment),

            DialogInterface.OnClickListener { dialog, which ->

                val comment = view.comment_et.text.toString()
                if (comment == "") {
                    return@OnClickListener
                }
                progressDialog = ProgressDialog.show(
                    mainActivity,
                    getString(R.string.please_wait),
                    getString(R.string.submitting_comment),
                    true
                )

                val apiInterface = APIClient.getClient(mainActivity).create(APIInterface::class.java)
                val call = apiInterface.postCommentOnly(Comment(plateNumberID, comment))
                call.enqueue(object : retrofit2.Callback<Comment> {

                    override fun onResponse(call: Call<Comment>, response: Response<Comment>) {

                        if (response.code() != Constant.OK_CODE) {

                            val resourceError = response.errorBody()
                            val errorMessage = Gson().fromJson(resourceError!!.string(), ErrorMessage::class.java)

                            defaultDialog(mainActivity, getString(R.string.please_try_again_tomorrow))

                            if (errorMessage.message == getString(R.string.unauthenticated)) {
                                mainActivity.startLoginActivity()
                            }
                        } else {

                            val resource = response.body()
                            val data = resource!!.data

                            val attachments = ArrayList<Comment.Attachments>()
                            val lastPos = (listComments.size - 1)
                            listComments.add( 0,
                                Comment(
                                    data!!.id!!,
                                    data.plateNumberId!!,
                                    data.comment!!,
                                    data.createdDate!!,
                                    data.userId!!,
                                    attachments
                                )
                            )

//                            listComments.add(
//                                Comment(
//                                    data!!.id!!,
//                                    data.plateNumberId!!,
//                                    data.comment!!,
//                                    data.createdDate!!,
//                                    data.userId!!,
//                                    attachments
//                                )
//                            )

                            val paths = ArrayList<String>()
                            paths.add(img1Path)
                            paths.add(img2Path)
                            paths.add(img3Path)

                            for (i in paths) {
                                if (i != "")
                                    imageTotal++
                            }

                            if (imageTotal == 0) {
                                commentAdapter.updateList(listComments,  mainView.comments_ctr, mainView.unlock_tv, mainView.not_available_tv, mainView.view_all_btn)
                                progressDialog.dismiss()
                            } else {
                                if (img1Path != "")
                                    uploadImage(data.id!!, img1Path, img1Bmp)

                                if (img2Path != "")
                                    uploadImage(data.id!!, img2Path, img2Bmp)

                                if (img3Path != "")
                                    uploadImage(data.id!!, img3Path, img3Bmp)
                            }

                        }
                    }

                    override fun onFailure(call: Call<Comment>, t: Throwable) {
                        RMPDialog.defaultDialog(mainActivity, getString(R.string.error_mes))
                    }

                })

            })

        builder.setNegativeButton(activity.getString(R.string.cancel), null)

        val dialog = builder.create()
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setView(view)
        dialog.show()

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        EasyImage.handleActivityResult(requestCode, resultCode, data, mainActivity, object : DefaultCallback() {
            override fun onImagePickerError(e: Exception?, source: EasyImage.ImageSource?, type: Int) {
                //Some error handling
            }

            fun onImagesPicked(imagesFiles: List<File>, source: EasyImage.ImageSource, type: Int) {
                //Handle the images
            }

            override fun onCanceled(source: EasyImage.ImageSource?, type: Int) {
                super.onCanceled(source, type)
            }

            override fun onImagePicked(imageFile: File?, source: EasyImage.ImageSource?, type: Int) {

                val bmp = BitmapFactory.decodeFile(imageFile!!.absolutePath)
                val resized =
                    ThumbnailUtils.extractThumbnail(BitmapFactory.decodeFile(imageFile.absolutePath), 400, 400);


                if (type == 1) {
                    img1Path = imageFile.absolutePath
                    img1Bmp = Util.modifyOrientation(bmp, img1Path)
                    val modifiedBmp = Util.modifyOrientation(resized, img1Path)
                    img1.setImageBitmap(modifiedBmp)

                } else if (type == 2) {
                    img2Path = imageFile.absolutePath
                    img2Bmp = Util.modifyOrientation(bmp, img2Path)
                    val modifiedBmp = Util.modifyOrientation(resized, img2Path)
                    img2.setImageBitmap(modifiedBmp)

                } else if (type == 3) {
                    img3Path = imageFile.absolutePath
                    img3Bmp = Util.modifyOrientation(bmp, img3Path)
                    val modifiedBmp = Util.modifyOrientation(resized, img3Path)
                    img3.setImageBitmap(modifiedBmp)

                }

            }
        })

    }

    private fun uploadImage(commentID: Int, path: String, bmp: Bitmap) {

        if (path == "") {
            return
        }

        val baos = ByteArrayOutputStream()
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos)
        val buf = baos.toByteArray()
        val requestBody = RequestBody
            .create(MediaType.parse("application/octet-stream"), buf)

        val apiInterface = APIClient.getClient(mainActivity).create(APIInterface::class.java)
        val call = apiInterface.uploadImage(commentID, requestBody)
        call.enqueue(
            object : retrofit2.Callback<Comment.Attachments> {

                override fun onResponse(call: Call<Comment.Attachments>, response: Response<Comment.Attachments>) {

                    if (response.code() != Constant.OK_CODE) {

                        val resourceError = response.errorBody()
                        val errorMessage = Gson().fromJson(resourceError!!.string(), ErrorMessage::class.java)

                        defaultDialog(mainActivity, getString(R.string.please_try_again_tomorrow))

                        if (errorMessage.message == getString(R.string.unauthenticated)) {
                            mainActivity.startLoginActivity()
                        }
                    } else {

                        val resource = response.body()

                        imageCtr++

                        val attachment = listComments.get(listComments.size - 1).commentAttachments
                        attachment!!.add(resource!!)

                        commentAdapter.updateList(listComments, mainView.comments_ctr, mainView.unlock_tv, mainView.not_available_tv, mainView.view_all_btn)

                        if (imageCtr == imageTotal) {
                            imageCtr = 0
                            imageTotal = 0
                            progressDialog.dismiss()
                            img1Path = ""
                            img2Path = ""
                            img3Path = ""
                        }

                    }
                }

                override fun onFailure(call: Call<Comment.Attachments>, t: Throwable) {
                    RMPDialog.defaultDialog(mainActivity, getString(R.string.error_mes))
                }

            })

    }

    private fun updateAlert(id: Int) {

        val apiInterface = APIClient.getClient(mainActivity).create(APIInterface::class.java)
        val call = apiInterface.updateAlert(Alert(id))
        call.enqueue(object : retrofit2.Callback<Alert> {

            override fun onResponse(call: Call<Alert>, response: Response<Alert>) {

                if (response.code() != Constant.OK_CODE) {

                    val resourceError = response.errorBody()
                    val errorMessage = Gson().fromJson(resourceError!!.string(), ErrorMessage::class.java)

                    defaultDialog(mainActivity, getString(R.string.please_try_again_tomorrow))

                    if (errorMessage.message == getString(R.string.unauthenticated)) {
                        mainActivity.startLoginActivity()
                    }
                } else {

                }
            }

            override fun onFailure(call: Call<Alert>, t: Throwable) {
                RMPDialog.defaultDialog(mainActivity, getString(R.string.error_mes))
            }

        })

    }


}