package com.publica.ratemyplate.fragment

import android.content.Context
import android.os.Bundle
import android.support.v4.app.FragmentActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import com.google.gson.Gson
import com.publica.ratemyplate.R
import com.publica.ratemyplate.activity.MainActivity
import com.publica.ratemyplate.adapter.HistoryAdapter
import com.publica.ratemyplate.data.Constant
import com.publica.ratemyplate.model.ErrorMessage
import com.publica.ratemyplate.model.History
import com.publica.ratemyplate.network.APIClient
import com.publica.ratemyplate.network.APIInterface
import com.publica.ratemyplate.ui.RMPDialog
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_history.view.*
import retrofit2.Call
import retrofit2.Response

class HistoryFragment : BaseFragment() {

    private lateinit var mainActivity: MainActivity
    private lateinit var mainView: View

    companion object {
        fun show(activity: FragmentActivity) {
            val fragment = HistoryFragment()
            fragment.clearPush(activity, activity.getString(R.string.history), "")

        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(R.layout.fragment_history, container, false)
        mainActivity = activity as MainActivity
        mainActivity.toolbar_title.text = getString(R.string.nav_history)
        mainView = view

        val imm = context!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(mainView.windowToken, 0)

        populateHistory(view)

        return view
    }

    fun populateHistory(view: View) {
        val apiInterface = APIClient.getClient(mainActivity).create(APIInterface::class.java)
        val call = apiInterface.getHistory("plate_number.comments", "plate_number.ratings")
        call.enqueue(object : retrofit2.Callback<History> {

            override fun onResponse(call: Call<History>, response: Response<History>) {

                if (response.code() != Constant.OK_CODE) {

                    val resourceError = response.errorBody()
                    val errorMessage = Gson().fromJson(resourceError!!.string(), ErrorMessage::class.java)

                    if (errorMessage.message == mainActivity.getString(R.string.unauthenticated)) {

                        mainActivity.startLoginActivity()
                    }
                } else {

                    val resource = response.body()!!
                    val historyData = resource.data!!

                    if (historyData.size > 1) {
                        mainView.not_available_tv.visibility = View.VISIBLE
                        mainView.view_all_btn.visibility = View.VISIBLE
                    }

                    val layoutManager = LinearLayoutManager(mainActivity)
                    view.history_recycler_view.layoutManager = layoutManager
                    val adapter = HistoryAdapter(mainActivity, historyData, object : HistoryAdapter.EventListener {
                        override fun onEvent() {
                            populateHistory(view)
                        }
                    })
                    view.history_recycler_view.adapter = adapter

                }
            }

            override fun onFailure(call: Call<History>, t: Throwable) {

                if (isAdded) {
                    RMPDialog.defaultDialog(mainActivity, getString(R.string.error_mes))
                }
            }

        })
    }

}