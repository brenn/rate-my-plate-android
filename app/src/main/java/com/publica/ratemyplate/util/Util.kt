package com.publica.ratemyplate.util

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Matrix
import android.media.ExifInterface
import android.support.v4.app.FragmentActivity
import android.widget.TextView
import java.io.File
import java.io.IOException
import java.util.*
import java.text.SimpleDateFormat
import java.util.Collections.rotate
import android.media.ExifInterface.ORIENTATION_NORMAL
import android.media.ExifInterface.TAG_ORIENTATION
import android.net.Uri


object Util {


    fun onDatePickerClick(textView: TextView, activity: FragmentActivity) {

        val c = Calendar.getInstance()
        val year1 = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)

        val datePickerDialog = DatePickerDialog(activity,
            DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                val dateDisplay = dayOfMonth.toString() + "-" + (monthOfYear + 1) + "-" + year
                textView.text = dateDisplay
            }, year1, month, day
        )
        datePickerDialog.datePicker.maxDate = System.currentTimeMillis()
        datePickerDialog.show()

    }

    fun onTimePickerClick(textView: TextView, activity: FragmentActivity) {

        val c = Calendar.getInstance()
        val hour = c.get(Calendar.HOUR_OF_DAY)
        val minute1 = c.get(Calendar.MINUTE)

        val timePickerDialog = TimePickerDialog(activity,
            TimePickerDialog.OnTimeSetListener { _, hourOfDay, minute ->

                //                val amPm: String
//                if (hourOfDay < 12) {
//                    amPm = "AM"
//                } else {
//                    amPm = "PM"
//                }

                val min: String
                if (minute < 10) {
                    min = "0$minute"
                } else {
                    min = minute.toString() + ""
                }

                val timeDisplay = hourOfDay.toString() + ":" + min
                textView.text = timeDisplay

            }, hour, minute1, true
        )

        timePickerDialog.show()

    }

    fun dateToEpoch(dateTime: String, dateOnly: Boolean): Long {

        var formatter = SimpleDateFormat("dd-MM-yyyy hh:mm", Locale.getDefault())
        if (dateOnly) {
            formatter = SimpleDateFormat("dd-MM-yyyy", Locale.getDefault())
        }

        if (dateTime == "" || dateTime == null) {
            return 0;
        }

        val gmt = formatter.parse(dateTime)
        val millisecondsSinceEpoch0 = gmt.time
        val asString = formatter.format(gmt)
        return millisecondsSinceEpoch0 / 1000

    }

    fun epochToDate(epochSeconds: Long): String {
        val updateDate = Date(epochSeconds * 1000)
        val format = SimpleDateFormat("dd-MM-yyyy", Locale.getDefault())
        return format.format(updateDate)
    }

    fun epochToDateAndTime(epochSeconds: Long): String {
        val updateDate = Date(epochSeconds * 1000)
        val format = SimpleDateFormat("dd-MM-yyyy HH:mm", Locale.getDefault())
        return format.format(updateDate)
    }


    @Throws(IOException::class)
    fun modifyOrientation(bitmap: Bitmap, imageAbsolutePath: String): Bitmap {
        val ei = ExifInterface(imageAbsolutePath)
        val orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL)

        when (orientation) {
            ExifInterface.ORIENTATION_ROTATE_90 -> return rotate(bitmap, 90f)

            ExifInterface.ORIENTATION_ROTATE_180 -> return rotate(bitmap, 180f)

            ExifInterface.ORIENTATION_ROTATE_270 -> return rotate(bitmap, 270f)

            ExifInterface.ORIENTATION_FLIP_HORIZONTAL -> return flip(bitmap, true, false)

            ExifInterface.ORIENTATION_FLIP_VERTICAL -> return flip(bitmap, false, true)

            else -> return bitmap
        }
    }

    fun rotate(bitmap: Bitmap, degrees: Float): Bitmap {
        val matrix = Matrix()
        matrix.postRotate(degrees)
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.width, bitmap.height, matrix, true)
    }

    fun flip(bitmap: Bitmap, horizontal: Boolean, vertical: Boolean): Bitmap {
        val matrix = Matrix()
        matrix.preScale((if (horizontal) -1 else 1).toFloat(), (if (vertical) -1 else 1).toFloat())
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.width, bitmap.height, matrix, true)
    }

    fun sendEmail(activity: FragmentActivity, `val`: String) {
        val emailIntent = Intent(
            Intent.ACTION_SENDTO, Uri.fromParts(
                "mailto", `val`, null
            )
        )
        emailIntent.putExtra(Intent.EXTRA_EMAIL, `val`) // String[] addresses

        activity.startActivity(Intent.createChooser(emailIntent, "Send email"))
    }

    fun sendEmail(activity: FragmentActivity, email: String, body: String, subject: String) {
        val emailIntent = Intent(
            Intent.ACTION_SENDTO, Uri.fromParts(
                "mailto", email, null
            )
        )
        emailIntent.putExtra(Intent.EXTRA_EMAIL, email) // String[] addresses
        emailIntent.putExtra(
            android.content.Intent.EXTRA_SUBJECT,
            subject
        );

        emailIntent.putExtra(
            android.content.Intent.EXTRA_TEXT,
            body
        );

        activity.startActivity(Intent.createChooser(emailIntent, "Send email"))
    }


    fun getDateNow(): String {
        val c = Calendar.getInstance().time
        val df = SimpleDateFormat("MM-dd-yyyy", Locale.getDefault())
        return df.format(c)
    }

    fun getTmeNow(): String {
        val c = Calendar.getInstance().time
        val df = SimpleDateFormat("hh:mm", Locale.getDefault())
        return df.format(c)
    }

}

