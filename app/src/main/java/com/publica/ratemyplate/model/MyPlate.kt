package com.publica.ratemyplate.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class MyPlate {

    @SerializedName("close_one")
    @Expose
    var closeOne: ArrayList<Other>? = null
    @SerializedName("others")
    @Expose
    var others: ArrayList<Other>? = null


    inner class Other {

        @SerializedName("id")
        @Expose
        var id: Int? = null
        @SerializedName("user_id")
        @Expose
        var userId: Int? = null
        @SerializedName("name")
        @Expose
        var name: String? = null
        @SerializedName("plate_type")
        @Expose
        var plateType: Int? = null
        @SerializedName("plate_number_id")
        @Expose
        var plateNumberId: Int? = null
        @SerializedName("plate_number")
        @Expose
        var plateNumber: PlateNumber? = null
        @SerializedName("created_at")
        @Expose
        var createdAt: Long? = null
        @SerializedName("updated_at")
        @Expose
        var updatedAt: Long? = null
        @SerializedName("my_alerts")
        @Expose
        var my_alerts: List<Alert>? = null

    }

    inner class CreatedAt_ {

        @SerializedName("date")
        @Expose
        var date: String? = null
        @SerializedName("timezone_type")
        @Expose
        var timezoneType: Int? = null
        @SerializedName("timezone")
        @Expose
        var timezone: String? = null

    }

    inner class UpdatedAt_ {

        @SerializedName("date")
        @Expose
        var date: String? = null
        @SerializedName("timezone_type")
        @Expose
        var timezoneType: Int? = null
        @SerializedName("timezone")
        @Expose
        var timezone: String? = null

    }

    inner class PlateNumber {

        @SerializedName("id")
        @Expose
        var id: Int? = null
        @SerializedName("plate_number")
        @Expose
        var plateNumber: String? = null
        @SerializedName("vehicle_type")
        @Expose
        var vehicleType: Any? = null
        @SerializedName("vehicle_color")
        @Expose
        var vehicleColor: Any? = null
        @SerializedName("user_id")
        @Expose
        var userId: Int? = null
        @SerializedName("created_at")
        @Expose
        var createdAt: Long? = null
        @SerializedName("updated_at")
        @Expose
        var updatedAt: Long? = null
        @SerializedName("ratings")
        @Expose
        var ratings: ArrayList<Rating>? = null
        @SerializedName("comments")
        @Expose
        var comments: ArrayList<Comment>? = null
        @SerializedName("alerts")
        @Expose
        var alerts: List<Alert>? = null

    }

}