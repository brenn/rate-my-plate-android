package com.publica.ratemyplate.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Moderate {

    constructor(id: Int, comment_id: Int, user_id: Int, createdAt: String, updatedAt: String, moderated: Int){
        this.id = id
        this.commentID = comment_id
        this.userId = user_id
        this.createdAt = createdAt
        this.updatedAt = updatedAt
        this.moderated = moderated
    }

    @SerializedName("success")
    @Expose
    var success: String? = null

    @SerializedName("data")
    @Expose
    var data: Data? = null

    @SerializedName("message")
    @Expose
    var message: String? = null
    @SerializedName("errors")
    @Expose
    var errors: ErrorMessage.Errors? = null
    @SerializedName("error")
    @Expose
    var error: String? = null
    @SerializedName("moderated")
    @Expose
    var moderated: Int? = null


    inner class Data{
        @SerializedName("id")
        @Expose
        var id: Int? = null
        @SerializedName("comment")
        @Expose
        var comment: String? = null
        @SerializedName("user_id")
        @Expose
        var userId: Int? = null
        @SerializedName("plate_number_id")
        @Expose
        var plateNumberId: Int? = null
        @SerializedName("created_at")
        @Expose
        var createdAt: String? = null
        @SerializedName("updated_at")
        @Expose
        var updatedAt: String? = null
        @SerializedName("created_date")
        @Expose
        var createdDate: String? = null
        @SerializedName("comment_id")
        @Expose
        var commentID: Int? = null

    }

    @SerializedName("id")
    @Expose
    var id: Int? = null
    @SerializedName("comment_id")
    @Expose
    var commentID: Int? = null
    @SerializedName("user_id")
    @Expose
    var userId: Int? = null
    @SerializedName("plate_number_id")
    @Expose
    var plateNumberId: Int? = null
    @SerializedName("created_at")
    @Expose
    var createdAt: String? = null
    @SerializedName("updated_at")
    @Expose
    var updatedAt: String? = null
    @SerializedName("created_date")
    @Expose
    var createdDate: String? = null


}