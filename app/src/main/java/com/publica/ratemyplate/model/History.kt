package com.publica.ratemyplate.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



//data class History(var plateNumber: String, var action: Int)


class History {

    @SerializedName("data")
    @Expose
    var data: ArrayList<Datum>? = null
    @SerializedName("message")
    @Expose
    var message: String? = null


    inner class Datum {

        @SerializedName("id")
        @Expose
        var id: Int? = null
        @SerializedName("alert_type_id")
        @Expose
        var alertTypeId: Int? = null
        @SerializedName("plate_number_id")
        @Expose
        var plateNumberId: Int? = null
        @SerializedName("comment_id")
        @Expose
        var comment_id: Int? = null
        @SerializedName("rating_id")
        @Expose
        var rating_id: Int? = null
        @SerializedName("rating_value")
        @Expose
        var rating_value: Int? = null
        @SerializedName("plate_number")
        @Expose
        var plate_number: PlateNumber.Datum? = null
        @SerializedName("created_at")
        @Expose
        var createdAt: MyPlate.CreatedAt_? = null
        @SerializedName("updated_at")
        @Expose
        var updatedAt: MyPlate.UpdatedAt_? = null

    }


}

