package com.publica.ratemyplate.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class Comment {

    constructor(comment_id: Int) {
        this.commentID = comment_id
    }

    constructor(plate_number_id: Int, comment: String) {
        this.plateNumberId = plate_number_id
        this.comment = comment
    }

    constructor(id: Int, plate_number_id: Int, comment: String, createdDate: String, userId: Int, attachments: ArrayList<Comment.Attachments>) {
        this.id = id
        this.plateNumberId = plate_number_id
        this.comment = comment
        this.createdDate = createdDate
        this.userId = userId
        this.commentAttachments = attachments

    }
    @SerializedName("comment_id")
    @Expose
    var commentID: Int? = null
    @SerializedName("id")
    @Expose
    var id: Int? = null

    @SerializedName("comment")
    @Expose
    var comment: String? = null

    @SerializedName("user_id")
    @Expose
    var userId: Int? = null

    @SerializedName("user")
    @Expose
    var user: User? = null

    @SerializedName("plate_number_id")
    @Expose
    var plateNumberId: Int? = null

    @SerializedName("comment_attachments")
    @Expose
    var commentAttachments: ArrayList<Attachments>? = null

    @SerializedName("moderate_comment")
    @Expose
    var moderateComment: Moderate? = null

    @SerializedName("created_at")
    @Expose
    var createdAt: MyPlate.CreatedAt_? = null

    @SerializedName("updated_at")
    @Expose
    var updatedAt: MyPlate.UpdatedAt_? = null
    @SerializedName("created_date")
    @Expose
    var createdDate: String? = null

    @SerializedName("success")
    @Expose
    var success: String? = null

    @SerializedName("data")
    @Expose
    var data: Comment? = null

    @SerializedName("message")
    @Expose
    var message: String? = null
    @SerializedName("errors")
    @Expose
    var errors: ErrorMessage.Errors? = null
    @SerializedName("error")
    @Expose
    var error: String? = null


    inner class Attachments {

        @SerializedName("id")
        @Expose
        var id: Int? = null
        @SerializedName("comment_id")
        @Expose
        var comment_id: Int? = null
        @SerializedName("name")
        @Expose
        var name: String? = null
        @SerializedName("image")
        @Expose
        var image: String? = null
        @SerializedName("thumbnail")
        @Expose
        var thumbnail: String? = null

        @SerializedName("created_at")
        @Expose
        var createdAt: MyPlate.CreatedAt_? = null
        @SerializedName("updated_at")
        @Expose
        var updatedAt: MyPlate.UpdatedAt_? = null

    }

    inner class User {

        @SerializedName("id")
        @Expose
        var id: Int? = null
        @SerializedName("name")
        @Expose
        var name: String? = null
        @SerializedName("email")
        @Expose
        var email: String? = null
        @SerializedName("nickname")
        @Expose
        var nickname: String? = null
        @SerializedName("gender")
        @Expose
        var gender: String? = null
        @SerializedName("age")
        @Expose
        var age: Int? = null
        @SerializedName("city")
        @Expose
        var city: String? = null
        @SerializedName("email_verified_at")
        @Expose
        var emailVerifiedAt: Any? = null
        @SerializedName("password")
        @Expose
        var password: String? = null
        @SerializedName("remember_token")
        @Expose
        var rememberToken: Any? = null
        @SerializedName("created_at")
        @Expose
        var createdAt: String? = null
        @SerializedName("updated_at")
        @Expose
        var updatedAt: String? = null
        @SerializedName("active")
        @Expose
        var active: Int? = null

    }


}