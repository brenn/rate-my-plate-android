package com.publica.ratemyplate.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class Rating {


    constructor(plate_number: String, name: String, plate_type: Int){
        this.plate_number = plate_number
        this.name = name
        this.plate_type = plate_type
    }

    constructor(name: String, plate_type: Int){
        this.name = name
        this.plate_type = plate_type
    }

    constructor(plate_number_id: Int, rating: Int){
        this.plateNumberID = plate_number_id
        this.rating = rating
    }

    constructor(
        plate_number: String,
        rating: Int,
        category: String,
        timing: Long,
        location: String,
        comment: String,
        add_to_my_plates: Int,
        name: String,
        plate_type: Int
    ) {

        this.plate_number = plate_number
        this.rating = rating
        this.category = category
        this.timing = timing
        this.location = location
        this.comment = comment
        this.add_to_my_plates = add_to_my_plates
        this.name = name
        this.plate_type = plate_type

    }

    @SerializedName("plate_number")
    @Expose
    var plate_number: String? = null
    @SerializedName("rating")
    @Expose
    var rating: Int? = null
    @SerializedName("category")
    @Expose
    var category: String? = null
    @SerializedName("timing")
    @Expose
    var timing: Long? = null
    @SerializedName("location")
    @Expose
    var location: String? = null
    @SerializedName("comment")
    @Expose
    var comment: String? = null
    @SerializedName("add_to_my_plates")
    @Expose
    var add_to_my_plates: Int? = null
    @SerializedName("name")
    @Expose
    var name: String? = null
    @SerializedName("plate_type")
    @Expose
    var plate_type: Int? = null
    @SerializedName("plate_number_id")
    @Expose
    var plateNumberID: Int? = null
    @SerializedName("message")
    @Expose
    var message: String? = null


    inner class Response {

        @SerializedName("success")
        @Expose
        var success: String? = null
        @SerializedName("data")
        @Expose
        var data: Data? = null

    }

    inner class Data {

        @SerializedName("rating")
        @Expose
        var rating: String? = null
        @SerializedName("category_id")
        @Expose
        var categoryId: String? = null
        @SerializedName("timing")
        @Expose
        var timing: String? = null
        @SerializedName("location")
        @Expose
        var location: String? = null
        @SerializedName("user_id")
        @Expose
        var userId: Int? = null
        @SerializedName("plate_number_id")
        @Expose
        var plateNumberId: Int? = null
//        @SerializedName("updated_at")
//        @Expose
//        var updatedAt: MyPlate.UpdatedAt_? = null
//        @SerializedName("created_at")
//        @Expose
//        var createdAt: MyPlate.CreatedAt_? = null
        @SerializedName("id")
        @Expose
        var id: Int? = null
        @SerializedName("comment_id")
        @Expose
        var commentId: Int? = null


        @SerializedName("updated_at")
        @Expose
        var updatedAt: Long? = null
        @SerializedName("created_at")
        @Expose
        var createdAt: Long? = null

//        @SerializedName("updated_at")
//        @Expose
//        var updatedAtObj: MyPlate.UpdatedAt_? = null
//        @SerializedName("created_at")
//        @Expose
//        var createdAtObj: MyPlate.CreatedAt_? = null

    }

}