package com.publica.ratemyplate.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Password {

    constructor(verification_code: String, password: String) {
        this.verification_code = verification_code
        this.password = password
    }

    constructor(email: String) {
        this.email = email
    }

    @SerializedName("email")
    @Expose
    var email: String? = null

    @SerializedName("verification_code")
    @Expose
    var verification_code: String? = null

    @SerializedName("password")
    @Expose
    var password: String? = null

    @SerializedName("message")
    @Expose
    var message: String? = null

}