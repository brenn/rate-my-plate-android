package com.publica.ratemyplate.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class Profile {

    constructor(email: String, nickname: String, firstName: String, lastName: String, gender: String, dateOfBirth: Int, city: String) {

        this.email = email
        this.nickname = nickname
        this.firstName = firstName
        this.lastName = lastName
        this.gender = gender
        this.age = dateOfBirth
        this.city = city

    }

    @SerializedName("id")
    @Expose
    var id: Int? = null
    @SerializedName("first_name")
    @Expose
    var firstName: String? = null
    @SerializedName("last_name")
    @Expose
    var lastName: String? = null
    @SerializedName("email")
    @Expose
    var email: String? = null
    @SerializedName("nickname")
    @Expose
    var nickname: String? = null
    @SerializedName("gender")
    @Expose
    var gender: String? = null
    @SerializedName("age")
    @Expose
    var age: Int? = null
    @SerializedName("city")
    @Expose
    var city: String? = null
    @SerializedName("email_verified_at")
    @Expose
    var emailVerifiedAt: Any? = null
    @SerializedName("password")
    @Expose
    var password: String? = null
    @SerializedName("remember_token")
    @Expose
    var rememberToken: Any? = null
    @SerializedName("created_at")
    @Expose
    var createdAt: String? = null
    @SerializedName("updated_at")
    @Expose
    var updatedAt: String? = null
    @SerializedName("active")
    @Expose
    var active: Int? = null
    @SerializedName("message")
    @Expose
    var message: String? = null
    @SerializedName("created_year")
    @Expose
    var createdYear: String? = null

}