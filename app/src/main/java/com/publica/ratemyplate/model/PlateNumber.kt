package com.publica.ratemyplate.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class PlateNumber {

    @SerializedName("data")
    @Expose
    var data: List<Datum>? = null
    @SerializedName("message")
    @Expose
    var message: String? = null

    inner class Datum {

        @SerializedName("id")
        @Expose
        var id: Int? = null
        @SerializedName("plate_number")
        @Expose
        var plateNumber: String? = null
        @SerializedName("vehicle_type")
        @Expose
        var vehicleType: Any? = null
        @SerializedName("vehicle_color")
        @Expose
        var vehicleColor: Any? = null
        @SerializedName("user_id")
        @Expose
        var userId: Int? = null
        @SerializedName("created_at")
        @Expose
        var createdAt: Long? = null
        @SerializedName("updated_at")
        @Expose
        var updatedAt: Long? = null
        @SerializedName("ratings")
        @Expose
        var ratings: ArrayList<Rating>? = null
        @SerializedName("comments")
        @Expose
        var comments: ArrayList<Comment>? = null
        @SerializedName("alerts")
        @Expose
        private val alerts: List<Alert>? = null

    }

    inner class Rating {

        @SerializedName("id")
        @Expose
        var id: Int? = null
        @SerializedName("user_id")
        @Expose
        var userId: Int? = null
        @SerializedName("username")
        @Expose
        var username: String? = null
        @SerializedName("rating")
        @Expose
        var rating: Int? = null
        @SerializedName("category")
        @Expose
        var category: String? = null
        @SerializedName("timing")
        @Expose
        var timing: Int? = null
        @SerializedName("location")
        @Expose
        var location: String? = null
        @SerializedName("plate_number_id")
        @Expose
        var plateNumberId: Int? = null
        @SerializedName("category_id")
        @Expose
        var categoryId: Int? = null
        @SerializedName("created_at")
        @Expose
        var createdAt: String? = null
        @SerializedName("updated_at")
        @Expose
        var updatedAt: String? = null

    }

}