package com.publica.ratemyplate.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class Alert {

    constructor(id: Int){
        this.id = id
    }

    @SerializedName("id")
    @Expose
    var id: Int? = null
    @SerializedName("alert_type_id")
    @Expose
    var alertTypeId: Int? = null
    @SerializedName("owner_id")
    @Expose
    var ownerId: Int? = null
    @SerializedName("plate_number_id")
    @Expose
    var plateNumberId: Int? = null
    @SerializedName("read")
    @Expose
    var read: Int? = null
    @SerializedName("created_at")
    @Expose
    var createdAt: String? = null
    @SerializedName("updated_at")
    @Expose
    var updatedAt: String? = null
    @SerializedName("message")
    @Expose
    var message: String? = null

}