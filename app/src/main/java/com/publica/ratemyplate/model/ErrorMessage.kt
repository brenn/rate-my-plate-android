package com.publica.ratemyplate.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ErrorMessage {

    @SerializedName("message")
    @Expose
    var message: String? = null
    @SerializedName("errors")
    @Expose
    var errors: Errors? = null
    @SerializedName("error")
    @Expose
    var error: String? = null

    class Errors {

        @SerializedName("email")
        @Expose
        var email: List<String>? = null
        @SerializedName("nickname")
        @Expose
        var nickname: List<String>? = null
        @SerializedName("first_name")
        @Expose
        var first_name: List<String>? = null
        @SerializedName("last_name")
        @Expose
        var last_name: List<String>? = null
        @SerializedName("gender")
        @Expose
        var gender: List<String>? = null
        @SerializedName("age")
        @Expose
        var age: List<String>? = null
        @SerializedName("city")
        @Expose
        var city: List<String>? = null
        @SerializedName("password")
        @Expose
        var password: List<String>? = null
    }

}