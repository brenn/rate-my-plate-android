package com.publica.ratemyplate.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Verify {

    constructor(verification_code: String){
        this.verification_code = verification_code
    }

    constructor(verification_code: String, password: String){
        this.verification_code = verification_code
        this.password = password
    }

    @SerializedName("verification_code")
    @Expose
    var verification_code: String? = null

    @SerializedName("message")
    @Expose
    var message: String? = null
    @SerializedName("password")
    @Expose
    var password: String? = null

}