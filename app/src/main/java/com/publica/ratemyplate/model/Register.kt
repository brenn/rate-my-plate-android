package com.publica.ratemyplate.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class Register {

    constructor(firstName: String, lastName: String, nickname: String, dateOfBirth: Int, city: String, gender: String, email: String, password: String) {
        this.firstName = firstName
        this.lastName = lastName
        this.nickname = nickname
        this.age = dateOfBirth
        this.city = city
        this.gender = gender
        this.email = email
        this.password = password
    }

    constructor(message: String){
        this.message = message
    }

    @SerializedName("first_name")
    @Expose
    var firstName: String? = null
    @SerializedName("last_name")
    @Expose
    var lastName: String? = null
    @SerializedName("email")
    @Expose
    var email: String? = null
    @SerializedName("nickname")
    @Expose
    var nickname: String? = null
    @SerializedName("password")
    @Expose
    var password: String? = null
    @SerializedName("gender")
    @Expose
    var gender: String? = null
    @SerializedName("age")
    @Expose
    var age: Int? = null
    @SerializedName("city")
    @Expose
    var city: String? = null
    @SerializedName("message")
    @Expose
    var message: String? = null

}