package com.publica.ratemyplate.model

data class Plate(var plateNumber: String)