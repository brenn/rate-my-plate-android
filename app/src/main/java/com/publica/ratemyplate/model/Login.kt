package com.publica.ratemyplate.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Login {

    constructor(email: String, password: String, log: String){
        this.email = email
        this.password = password
        this.log = log
    }

    @SerializedName("email")
    @Expose
    var email: String? = null
    @SerializedName("password")
    @Expose
    var password: String? = null
    @SerializedName("log")
    @Expose
    var log: String? = null

    @SerializedName("access_token")
    @Expose
    val accessToken: String? = null
    @SerializedName("token_type")
    @Expose
    val tokenType: String? = null
    @SerializedName("expires_in")
    @Expose
    val expiresIn: Int? = null
    @SerializedName("id")
    @Expose
    val id: Int? = null

}