package com.publica.ratemyplate.model

import com.publica.ratemyplate.model.PlateNumber.Datum
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class Category {

    @SerializedName("data")
    @Expose
    var data: List<Datum>? = null

    inner class Datum {

        @SerializedName("id")
        @Expose
        var id: Int? = null
        @SerializedName("name")
        @Expose
        var name: String? = null
        @SerializedName("created_at")
        @Expose
        var createdAt: Any? = null
        @SerializedName("updated_at")
        @Expose
        var updatedAt: Any? = null

    }

}